<?php

use PHPUnit\Framework\TestCase;;

use PHPUnit\DbUnit\TestCaseTrait;

use GuzzleHttp\Client;
use DI\ContainerBuilder;

use PHPUnit\DbUnit\DataSet\CsvDataSet;

class DocenteTest extends TestCase
{
    use TestCaseTrait;
    static private $pdo = null;

    private $conn = null;
    private $container;
    public function setUp()
    {
        $builder = new ContainerBuilder();


        $builder->addDefinitions(
            [
                PDO::class => function (ContainerInterface $container) {
                    $host = "anapesdtanadiputada.com";
                    $dbname =  "anapesta_devel";
                    $username = "anapesta_devel";
                    $password = "anapesta_devel";
                    $charset = "utf8";
                    $flags = [
                        // Turn off persistent connections
                        PDO::ATTR_PERSISTENT => false,
                        // Enable exceptions
                        PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
                        // Emulate prepared statements
                        PDO::ATTR_EMULATE_PREPARES => true,
                        // Set default fetch mode to array
                        PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
                        // Set character set
                        PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8 COLLATE utf8_unicode_ci'
                    ];
                    $dsn = "mysql:host=$host;dbname=$dbname;charset=$charset";

                    return new PDO($dsn, $username, $password, $flags);
                }
            ]
        );
        $this->container = $builder->build();

        // $this->container->set('Aplication\infraestructure_interfaces\DocenteDaoInterface', DI\get('Infrastructure\mysql\adapter\DocenteDao2'));

        $this->container->injectOn($this);

        parent::setUp();
    }

    final public function getConnection()
    {
        $host = "localhost";
        $dbname =  "registros";
        $username = "root";
        $password = "";
        $charset = "utf8mb4";

        $dsn = "mysql:host=$host;dbname=$dbname;charset=$charset";


        if ($this->conn === null) {
            if (self::$pdo == null) {
                self::$pdo = new PDO($dsn, $username, $password);
            }
            $this->conn = $this->createDefaultDBConnection(self::$pdo, $dbname);
        }

        return $this->conn;
    }

    protected function getDataSet()
    {
        $dataSet = new CsvDataSet();
        $dataSet->addTable('en_docentes', dirname(__FILE__) . "../../sql/csv/0004_en_docentes.csv");
        return $dataSet;
    }

    public function testTodoGet()
    {
        $this->assertTrue(true);
    }

    public function testPOST()
    {
        $client = new Client([
            'base_uri' => 'http://localhost',
            'timeout'  => 20.0,
        ]);

        $response = $client->post('/devel/api/docente/listar');
        $expected = $this->getDataSet()->getTable("en_docentes")->get;
        print_r($expected);
        exit;
        $this->assertEquals(200, $response->getStatusCode());
    }
}
