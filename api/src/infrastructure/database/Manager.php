<?php

namespace Infrastructure\database;

use Illuminate\Database\Capsule\Manager as Capsule;
use Illuminate\Database\Connectors\ConnectionFactory;

use Illuminate\Database\Query\Grammars\MySqlGrammar as QueryGrammar;
use Illuminate\Database\Schema\Grammars\MySqlGrammar as SchemaGrammar;

class Manager extends Capsule
{
    protected function setupManager()
    {
        parent::setupManager(); //
        $app = $this->container;


        $this->manager->extend('mysql', function ($config, $name) use ($app) {
            //Create default connection from factory
            $factory = new ConnectionFactory($app);
            $connection = $factory->make($config, $name);

            //Instantiate our connection with the default connection data
            $new_connection = new Connection(
                $connection->getPdo(),
                $connection->getDatabaseName(),
                $connection->getTablePrefix(),
                $config
            );
            //Set the appropriate grammar object
            $new_connection->setQueryGrammar(new QueryGrammar());
            $new_connection->setSchemaGrammar(new SchemaGrammar());
            return $new_connection;
        });
    }
}
