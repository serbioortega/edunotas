<?php

namespace Infrastructure\database;

class Connection extends \Illuminate\Database\MySqlConnection {
    public function query() {
        return new Builder(
            $this,
            $this->getQueryGrammar(),
            $this->getPostProcessor()
        );
    }
}
