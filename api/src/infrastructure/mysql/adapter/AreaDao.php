<?php

namespace Infrastructure\mysql\adapter;

use Aplication\infraestructure_interfaces\AreaDaoInterface;
use Infrastructure\database\Manager as DB;

class AreaDao implements AreaDaoInterface
{
    protected $db;

    public function __construct(DB $db)
    {
        $this->db = $db;
    }

    public function todos()
    {
        return $this->db->table('en_areas')->get();
    }

    public function obtener($data)
    {
        $idArea = $data["id_area"];
        $data = $this->db->table('en_areas')->where('id_area', '=', $idArea)->first();

        return $data;
    }
}
