<?php

namespace Infrastructure\mysql\adapter;

use Aplication\infraestructure_interfaces\DocenteDaoInterface;
use Infrastructure\database\Manager as DB;

class DocenteDao extends Dao implements DocenteDaoInterface
{
    protected $db;

    public function __construct(DB $db)
    {
        $this->db = $db;
    }

    public function todosPorColegio($data)
    {
        $idColegio = $data['id_colegio'];

        $sql = "SELECT *, concat(nombres, ' ', apellidos) AS nombre_completo
                FROM en_docentes d
                JOIN en_usuarios u ON u.id_usuario = d.id_usuario
                WHERE u.id_colegio = :id_colegio";

        return DB::SELECT($sql, ["id_colegio" => $idColegio]);
    }

    public function obtener($data)
    {
        $idColegio = $data['id_colegio'];
        $idDocente = $data['id_docente'];

        $data = $this->db->table('en_docentes AS d')
            ->join("en_usuarios AS u", "u.id_usuario", "=", "d.id_usuario")
            ->where('d.id_docente', '=', $idDocente)
            ->get();

        if (count($data) > 0) {
            $data = $data[0];
        }
        return $data;
    }

    public function guardarGetId($data)
    {
        $idDocente = $this->db->table('en_docentes')->insertGetId($data);
        return $idDocente;
    }

    public function actualizarGetId($data)
    {
        $idDocente = $data['id_docente'];
        $this->db->table('en_docentes')->where('id_docente', '=', $idDocente)->update($data);
        return $idDocente;
    }

    public function eliminar($data)
    {
        $data = $this->db->table('en_docentes')->where('id_docente', '=', $data["id_docente"])->delete();
        return $data;
    }
}
