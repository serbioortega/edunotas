<?php

namespace Infrastructure\mysql\adapter;

use Aplication\infraestructure_interfaces\DaoInterface;
use Infrastructure\database\Manager as DB;

class Dao implements DaoInterface
{
    protected $db;

    public function __construct(DB $db)
    {
        $this->db = $db;
    }

    public function beginTransaction()
    {
        DB::beginTransaction();
    }

    public function commit()
    {
        DB::commit();
    }

    public function rollback()
    {
        DB::rollback();
    }
}
