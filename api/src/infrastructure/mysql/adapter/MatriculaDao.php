<?php

namespace Infrastructure\mysql\adapter;

use Aplication\infraestructure_interfaces\MatriculaDaoInterface;
use Infrastructure\database\Manager as DB;

class MatriculaDao extends Dao implements MatriculaDaoInterface
{
    protected $db;

    public function __construct(DB $db)
    {
        $this->db = $db;
    }

    public function todosPorColegio($data)
    {
        $idColegio = $data['id_colegio'];

        $sql = "SELECT *, concat(nombres, ' ', apellidos) AS nombre_completo
                FROM en_matriculas d
                JOIN en_usuarios u ON u.id_usuario = d.id_usuario
                WHERE u.id_colegio = :id_colegio";

        return DB::SELECT($sql, ["id_colegio" => $idColegio]);
                    

    }

    public function obtener($data)
    {
        $idColegio = $data['id_colegio'];
        $idMatricula = $data['id_matricula'];

        $data = $this->db->table('en_matriculas')
                    ->where('id_colegio', '=', $idColegio)
                    ->where('id_matricula', '=', $idMatricula)
                    ->first();

        return $data;
    }

    public function guardarGetId($matricula)
    {
        $idMatricula = $this->db->table('en_matriculas')->insertGetId($matricula);
        return $idMatricula;
    }
 
    public function actualizarGetId($data)
    {
        $idColegio = $data['id_colegio'];
        $idMatricula = $data['id_matricula'];

        $this->db->table('en_matriculas')
                ->where('id_colegio', '=', $idColegio)
                ->where('id_matricula', '=', $idMatricula)
                ->update($data);

        return $idMatricula;
    }

    public function eliminar($data)
    {
        $idColegio = $data['id_colegio'];
        $idMatricula = $data['id_matricula'];

        $data = $this->db->table('en_matriculas')
                    ->where('id_colegio', '=', $idColegio)
                    ->where('id_matricula', '=', $idMatricula)
                    ->delete();
        return $data;
    }
}
