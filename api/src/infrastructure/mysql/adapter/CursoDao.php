<?php

namespace Infrastructure\mysql\adapter;

use Aplication\infraestructure_interfaces\CursoDaoInterface;
use Infrastructure\database\Manager as DB;

class CursoDao implements CursoDaoInterface
{
    protected $db;

    public function __construct(DB $db)
    {
        $this->db = $db;
    }

    public function todosPorColegio($data)
    {
        $idColegio = $data['id_colegio'];

        $sql = 'SELECT
                    c.id_curso,
                    CONCAT(g.nombre_grado, " ", c.grupo) AS nombre_curso,
                    c.id_jornada,
                    j.nombre_jornada,
                    c.id_sede,
                    s.nombre_sede,
                    CONCAT(d.nombres, " ", d.apellidos) AS docente
                FROM en_cursos c
                JOIN en_grados g ON g.id_grado = c.id_grado
                JOIN en_jornadas j ON j.id_jornada = c.id_jornada
                JOIN en_sedes s ON s.id_sede = c.id_sede
                JOIN en_docentes d ON d.id_docente = c.id_docente
                WHERE c.id_colegio = :id_colegio';

        return DB::SELECT($sql, ["id_colegio" => $idColegio]);
    }

    public function obtener($data)
    {
        $idColegio = $data['id_colegio'];
        $idCurso = $data['id_curso'];

        $data = $this->db->table('en_cursos')
            ->where('id_colegio', '=', $idColegio)
            ->where('id_curso', '=', $idCurso)
            ->first();

        return $data;
    }

    public function guardarGetId($data)
    {
        $idCurso = $this->db->table('en_cursos')->insertGetId($data);

        return $idCurso;
    }

    public function actualizarGetId($data)
    {
        $idColegio = $data['id_colegio'];
        $idCurso = $data['id_curso'];

        $this->db->table('en_cursos')
            ->where('id_colegio', '=', $idColegio)
            ->where('id_curso', '=', $idCurso)
            ->update($data);

        return $idCurso;
    }

    public function eliminar($data)
    {
        $idColegio = $data['id_colegio'];
        $idCurso = $data['id_curso'];

        $data = $this->db->table('en_cursos')
            ->where('id_colegio', '=', $idColegio)
            ->where('id_curso', '=', $idCurso)
            ->delete();

        return $data;
    }
}
