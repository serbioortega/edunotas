<?php

namespace Infrastructure\mysql\adapter;

use Aplication\infraestructure_interfaces\LogroDaoInterface;
use Infrastructure\database\Manager as DB;

class LogroDao implements LogroDaoInterface
{
    protected $db;

    public function __construct(DB $db)
    {
        $this->db = $db;
    }

    public function todosPorColegio($data)
    {
        $idColegio = $data['id_colegio'];

        return $this->db->table('en_logros')
                    ->where('id_colegio', '=', $idColegio)
                    ->get();
    }

    public function obtener($data)
    {
        $idColegio = $data['id_colegio'];
        $idLogro = $data['id_logro'];

        $data = $this->db->table('en_logros')
                    ->where('id_colegio', '=', $idColegio)
                    ->where('id_logro', '=', $idLogro)
                    ->first();

        return $data;
    }

    public function guardarGetId($data)
    {
        $idLogro = $this->db->table('en_logros')->insertGetId($data);
        return $idLogro;
    }

    public function actualizarGetId($data)
    {
        $idColegio = $data['id_colegio'];
        $idLogro = $data['id_logro'];

        $this->db->table('en_logros')
                    ->where('id_colegio', '=', $idColegio)
                    ->where('id_logro', '=', $idLogro)
                    ->update($data);

        return $idLogro;
    }

    public function eliminar($data)
    {
        $idColegio = $data['id_colegio'];
        $idLogro = $data['id_logro'];

        $data = $this->db->table('en_logros')
                    ->where('id_colegio', '=', $idColegio)
                    ->where('id_logro', '=', $idLogro)
                    ->delete();

        return $data;
    }
}
