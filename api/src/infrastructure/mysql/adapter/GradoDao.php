<?php

namespace Infrastructure\mysql\adapter;

use Aplication\infraestructure_interfaces\GradoDaoInterface;
use Infrastructure\database\Manager as DB;

class GradoDao implements GradoDaoInterface
{
    protected $db;

    public function __construct(DB $db)
    {
        $this->db = $db;
    }

    public function todos()
    {
        return $this->db->table('en_grados')->get();
    }

    public function obtener($data)
    {
        $idGrado = $data["id_grado"];

        $data = $this->db->table('en_grados')
                    ->where('id_grado', '=', $idGrado)
                    ->first();

        return $data;
    }

}
