<?php

namespace Infrastructure\mysql\adapter;

use Aplication\infraestructure_interfaces\ColegioDaoInterface;
use Infrastructure\database\Manager as DB;

class ColegioDao implements ColegioDaoInterface
{
    protected $db;

    public function __construct(DB $db)
    {
        $this->db = $db;
    }

    public function todos()
    {
        return $this->db->table('en_colegios')->get();
    }

    public function obtener($idColegio)
    {
        $data = $this->db->table('en_colegios')->where('id_colegio', '=', $idColegio)->get();
        if (count($data) > 0) {
            $data = $data[0];
        }
        return $data;
    }

    public function guardarGetId($colegio)
    {
        $idColegio = $this->db->table('en_colegios')->insertGetId($colegio);
        return $idColegio;
    }

    public function actualizarGetId($colegio)
    {
        $idColegio = $colegio['id_colegio'];
        $this->db->table('en_colegios')->where('id_colegio', '=', $idColegio)->update($colegio);
        return $idColegio;
    }

    public function eliminar($idColegio)
    {
        $data = $this->db->table('en_colegios')->where('id_colegio', '=', $idColegio)->delete();
        return $data;
    }
}
