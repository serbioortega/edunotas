<?php

namespace Infrastructure\mysql\adapter;

use Aplication\infraestructure_interfaces\JornadaDaoInterface;
use Infrastructure\database\Manager as DB;

class JornadaDao implements JornadaDaoInterface
{
    protected $db;

    public function __construct(DB $db)
    {
        $this->db = $db;
    }

    public function todos()
    {
        return $this->db->table('en_jornadas')->get();
    }

    public function todosConSede($data)
    {

        $idColegio = $data['id_colegio'];

        $sql = "SELECT DISTINCT j.id_jornada, j.nombre_jornada, c.id_sede
                FROM en_jornadas j
                JOIN en_cursos c ON c.id_jornada = j.id_jornada
                WHERE c.id_colegio = :id_colegio
                ";
        return DB::SELECT($sql, ["id_colegio" => $idColegio]);
    }

    public function obtener($data)
    {
        $idJornada = $data['id_jornada'];

        $data = $this->db->table('en_jornadas')
            ->where('id_jornada', '=', $idJornada)
            ->first();

        return $data;
    }
}
