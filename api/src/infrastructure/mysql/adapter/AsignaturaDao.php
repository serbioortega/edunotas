<?php

namespace Infrastructure\mysql\adapter;

use Aplication\infraestructure_interfaces\AsignaturaDaoInterface;
use Infrastructure\database\Manager as DB;

class AsignaturaDao implements AsignaturaDaoInterface
{
    protected $db;

    public function __construct(DB $db)
    {
        $this->db = $db;
    }

    public function todosPorColegio($data)
    {
        $idColegio = $data['id_colegio'];

        $sql = 'SELECT
                    a.id_asignatura,
                    a.id_materia,
                    a.nombre_materia,
                    a.id_curso,
                    c.curso,
                    c.nombre_jornada,
                    c.nombre_sede,
                    a.id_docente,
                    a.docente
                FROM view_asignaturas a
                JOIN view_cursos c ON c.id_curso = a.id_curso';

        return DB::SELECT($sql);
    }

    public function obtener($data)
    {
        $idColegio = $data['id_colegio'];
        $idAsignatura = $data['id_asignatura'];

        $data = $this->db->table('en_asignaturas')->where('id_asignatura', '=', $idAsignatura)->first();

        return $data;
    }

    public function guardarGetId($asignatura)
    {
        $idAsignatura = $this->db->table('en_asignaturas')->insertGetId($asignatura);
        return $idAsignatura;
    }

    public function actualizarGetId($data)
    {
        $idColegio = $data['id_colegio'];
        $idAsignatura = $data['id_asignatura'];

        $this->db->table('en_asignaturas')->where('id_asignatura', '=', $idAsignatura)->update($data);
        return $idAsignatura;
    }

    public function eliminar($data)
    {
        $idColegio = $data['id_colegio'];
        $idAsignatura = $data['id_asignatura'];

        $data = $this->db->table('en_asignaturas')->where('id_asignatura', '=', $idAsignatura)->delete();
        return $data;
    }
}
