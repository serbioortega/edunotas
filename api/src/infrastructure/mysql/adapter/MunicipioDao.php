<?php

namespace Infrastructure\mysql\adapter;

use Aplication\infraestructure_interfaces\MunicipioDaoInterface;
use Infrastructure\database\Manager as DB;

class MunicipioDao implements MunicipioDaoInterface
{
    protected $db;

    public function __construct(DB $db)
    {
        $this->db = $db;
    }

    public function todosDepartamentos()
    {
        $data = $this->db->table('en_municipios')->groupby('codigo_departamento')->get();
        return $data;
    }

    public function municipiosDelDepartamento($codigoDepartamento)
    {
        $data = $this->db->table('en_municipios')->where('codigo_departamento', '=', $codigoDepartamento)->get();
        return $data;
    }
}
