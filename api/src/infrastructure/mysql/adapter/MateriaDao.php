<?php

namespace Infrastructure\mysql\adapter;

use Aplication\infraestructure_interfaces\MateriaDaoInterface;
use Infrastructure\database\Manager as DB;

class MateriaDao implements MateriaDaoInterface
{
    protected $db;

    public function __construct(DB $db)
    {
        $this->db = $db;
    }

    public function todosPorColegio($data)
    {
        $idColegio = $data['id_colegio'];

        $sql = 'SELECT
                    m.id_materia,
                    m.nombre_materia,
                    a.nombre_area
                FROM en_materias m
                JOIN en_areas a ON a.id_area = m.id_area
                WHERE m.id_colegio = :id_colegio';

        return DB::SELECT($sql, ["id_colegio" => $idColegio]);
    }

    public function obtener($data)
    {
        $idMateria = $data["id_materia"];

        $data = $this->db->table('en_materias')
            ->where('id_materia', '=', $idMateria)
            ->first();

        return $data;
    }

    public function guardarGetId($data)
    {
        $idMateria = $this->db->table('en_materias')->insertGetId($data);
        return $idMateria;
    }

    public function actualizarGetId($data)
    {
        $idMateria = $data['id_materia'];

        $this->db->table('en_materias')
            ->where('id_materia', '=', $idMateria)
            ->update($data);

        return $idMateria;
    }

    public function obtenerDetalle($data)
    {
        $idMateria = $data["id_materia"];

        $data = $this->db->table('en_materias_det')
            ->where('id_materia', '=', $idMateria)
            ->get();

        return $data;
    }

    public function guardarDetalle($data)
    {
        $this->db->table('en_materias_det')->insert($data);
    }

    public function actualizarDetalle($data)
    {
        $idMateriaDet = $data['id_materia_det'];

        $this->db->table('en_materias_det')
            ->where('id_materia_det', '=', $idMateriaDet)
            ->update($data);

        return $idMateriaDet;
    }

    public function eliminar($data)
    {
        $idMateria = $data['id_materia'];

        $data = $this->db->table('en_materias')
            ->where('id_materia', '=', $idMateria)
            ->delete();

        return $data;
    }

    public function eliminarNotInDetalle($data)
    {
        $data = $this->db->table('en_materias_det')
            ->whereNotIn('id_materia_det', $data)
            ->delete();

        return $data;
    }
}
