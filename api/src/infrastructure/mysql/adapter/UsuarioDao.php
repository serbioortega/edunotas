<?php

namespace Infrastructure\mysql\adapter;

use Aplication\infraestructure_interfaces\UsuarioDaoInterface;
use Infrastructure\database\Manager as DB;

class UsuarioDao extends Dao implements UsuarioDaoInterface
{
    protected $db;

    public function __construct(DB $db)
    {
        $this->db = $db;
    }

    public function todosPorColegio($data)
    {
        $idColegio = $data['id_colegio'];
        
        return $this->db->table('en_usuarios')->where('id_colegio', '=', $idColegio)->get();
    }

    public function obtener($data)
    {
        $idColegio = $data['id_colegio'];
        $idUsuario = $data['id_usuario'];

        $data = $this->db->table('en_usuarios')
                ->where('id_usuario', '=', $idUsuario)
                ->where('id_colegio', '=', $idColegio)
                ->first();

        return $data;
    }

    public function guardarGetId($data)
    {
        $idUsuario = $this->db->table('en_usuarios')->insertGetId($data);
        return $idUsuario;
    }

    public function actualizarGetId($data)
    {
        $idColegio = $data['id_colegio'];
        $idUsuario = $data['id_usuario'];

        $this->db->table('en_usuarios')
            ->where('id_colegio', '=', $idColegio)
            ->where('id_usuario', '=', $idUsuario)
            ->update($data);

        return $idUsuario;
    }

    public function eliminar($data)
    {
        $idColegio = $data['id_colegio'];
        $idUsuario = $data['id_usuario'];

        $data = $this->db->table('en_usuarios')
                    ->where('id_colegio', '=', $idColegio)
                    ->where('id_usuario', '=', $idUsuario)
                    ->delete();

        return $data;
    }
}
