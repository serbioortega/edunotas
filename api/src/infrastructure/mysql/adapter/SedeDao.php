<?php

namespace Infrastructure\mysql\adapter;

use Aplication\infraestructure_interfaces\SedeDaoInterface;
use Infrastructure\database\Manager as DB;

class SedeDao implements SedeDaoInterface
{
    protected $db;

    public function __construct(DB $db)
    {
        $this->db = $db;
    }

    public function todosPorColegio($data)
    {
        $idColegio = $data['id_colegio'];

        $data = $this->db->table('en_sedes')->where('id_colegio', '=', $idColegio)->get()->toArray();

        return $data;
    }

    public function obtener($data)
    {
        $idColegio = $data['id_colegio'];
        $idSede = $data['id_sede'];

        $data = $this->db->table('en_sedes')
            ->where('id_sede', '=', $idSede)
            ->where('id_colegio', '=', $idColegio)
            ->first();


        return $data;
    }

    public function guardarGetId($data)
    {
        $idSede = $this->db->table('en_sedes')->insertGetId($data);
        return $idSede;
    }

    public function actualizarGetId($data)
    {
        $idColegio = $data['id_colegio'];
        $idSede = $data['id_sede'];

        $this->db->table('en_sedes')
            ->where('id_sede', '=', $idSede)
            ->where('id_colegio', '=', $idColegio)
            ->update($data);

        return $idSede;
    }

    public function eliminarSedesDiferentes($data)
    {
        $idColegio = $data['id_colegio'];
        $idSede = $data['id_sede'];

        $this->db->table('en_sedes')
            ->where('id_colegio', '=', $idColegio)
            ->whereNotIn('id_sede', $idSede)
            ->delete();
    }
}
