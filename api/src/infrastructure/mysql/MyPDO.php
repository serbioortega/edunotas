<?php

namespace Infrastructure\mysql;

class MyPDO extends \PDO
{

    public function __construct($dsn, $username, $password, $options)
    {

        parent::__construct($dsn, $username, $password, $options);
    }

    public function all($sql)
    {
        $r = $this->prepare($sql);
        $r->execute();

        return $r->fetchAll();
    }
}
