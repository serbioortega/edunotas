<?php

use Infrastructure\http\controller\AsignaturaController;
use Slim\Routing\RouteCollectorProxy;

use Infrastructure\http\controller\ColegioController;
use Infrastructure\http\controller\CursoController;
use Infrastructure\http\controller\DocenteController;
use Infrastructure\http\controller\LogroController;
use Infrastructure\http\controller\MateriaController;
use Infrastructure\http\controller\MatriculaController;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

$app->group('/colegio', function (RouteCollectorProxy $group) {
    $group->any('/listar', ColegioController::class . ':listar');
    $group->any('/recursos', ColegioController::class . ':recursos');
    $group->any('/guardar', ColegioController::class . ':guardar');
    $group->any('/eliminar', ColegioController::class . ':eliminar');
});

$app->group('/docente', function (RouteCollectorProxy $group) {
    $group->any('/listar', DocenteController::class . ':listar');
    $group->any('/recursos', DocenteController::class . ':recursos');
    $group->any('/guardar', DocenteController::class . ':guardar');
    $group->any('/eliminar', DocenteController::class . ':eliminar');
});

$app->group('/materia', function (RouteCollectorProxy $group) {
    $group->any('/listar', MateriaController::class . ':listar');
    $group->any('/recursos', MateriaController::class . ':recursos');
    $group->any('/guardar', MateriaController::class . ':guardar');
    $group->any('/eliminar', MateriaController::class . ':eliminar');
});

$app->group('/curso', function (RouteCollectorProxy $group) {
    $group->any('/listar', CursoController::class . ':listar');
    $group->any('/recursos', CursoController::class . ':recursos');
    $group->any('/guardar', CursoController::class . ':guardar');
    $group->any('/eliminar', CursoController::class . ':eliminar');
});

$app->group('/asignatura', function (RouteCollectorProxy $group) {
    $group->any('/listar', AsignaturaController::class . ':listar');
    $group->any('/recursos', AsignaturaController::class . ':recursos');
    $group->any('/guardar', AsignaturaController::class . ':guardar');
    $group->any('/eliminar', AsignaturaController::class . ':eliminar');
});

$app->group('/logro', function (RouteCollectorProxy $group) {
    $group->any('/listar', LogroController::class . ':listar');
    $group->any('/recursos', LogroController::class . ':recursos');
    $group->any('/guardar', LogroController::class . ':guardar');
    $group->any('/eliminar', LogroController::class . ':eliminar');
});

$app->group('/matricula', function (RouteCollectorProxy $group) {
    $group->any('/listar', MatriculaController::class . ':listar');
    $group->any('/recursos', MatriculaController::class . ':recursos');
    $group->any('/guardar', MatriculaController::class . ':guardar');
    $group->any('/eliminar', MatriculaController::class . ':eliminar');
});

function importarCsv($file)
{
    $nombreColumnas = array();
    $registros = array();

    $linea = -1;
    $archivo = fopen($file, "r");
    while (($datos = fgetcsv($archivo, 0, ";", "'")) == true) {
        $linea++;
        $num = count($datos);

        if ($linea == 0) {
            for ($columna = 0; $columna < $num; $columna++) {
                $nombreColumnas[] = $datos[$columna];
            }
            continue;
        }

        $linea++;

        $registroTemp = array();

        for ($columna = 0; $columna < $num; $columna++) {
            $registroTemp[$nombreColumnas[$columna]] = $datos[$columna] == "" ? null : $datos[$columna];
        }

        $registros[] = $registroTemp;
    }

    fclose($archivo);

    return $registros;
}



function importarSql($file)
{
    $sentencias = array();

    $lines = file($file);
    $sql = '';
    foreach ($lines as $line) {
        if (substr(trim($line), 0, 2) == '--' || $line == '' || substr(trim($line), 0, 2) == '/*')
            continue;

        $sql .= $line;

        if (substr(trim($line), -1, 1) == ';') {
            $sentencias[] = $sql;
            $sql = '';
        }
    }

    return $sentencias;
}

$app->get('/db', function (Request $request, Response $response, $args) use ($db) {

    $schema = $db->schema();
    // $schema->dropIfExists('en_colegios');

    $rutasArchivosSql = glob(__DIR__ . "\\..\\..\\..\\sql\\sql\\*.sql");
    foreach ($rutasArchivosSql as $path_fichero) {
        $sentencias = importarSql($path_fichero);
        foreach ($sentencias as $sql) {
            $schema->getConnection()->statement($sql);
        }
    }

    $rutasArchivosCsv = glob(__DIR__ . "\\..\\..\\..\\sql\\csv\\*.csv");
    foreach ($rutasArchivosCsv as $path_fichero) {
        $registros = importarCsv($path_fichero);
        $info_archivo = pathinfo($path_fichero);
        $nombre_tabla = substr($info_archivo['filename'], 5);
        foreach ($registros as $registro) {
            $db->table($nombre_tabla)->insertGetId($registro);
        }
    }

    $response->getBody()->write('ll');
    return $response;
});
