<?php

namespace Infrastructure\http\controller;

use Psr\Container\ContainerInterface;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Aplication\services\CursoServiceInterface;


class CursoController
{
     protected $container;
     protected $cursoService;

     public function __construct(ContainerInterface $container, CursoServiceInterface $cursoService)
     {
          $this->container = $container;
          $this->cursoService = $cursoService;
     }

     public function listar(Request $request, Response $response, $args)
     {
          $data = $request->getParsedBody();
          $data = $this->cursoService->listar($data);

          $payload = json_encode($data);
          $response->getBody()->write($payload);

          return $response;
     }

     public function recursos(Request $request, Response $response, $args)
     {
          $data = $request->getParsedBody();

          $recursos = $this->cursoService->obtenerRecursos($data);

          $payload = json_encode($recursos);
          $response->getBody()->write($payload);

          return $response;
     }

     public function guardar(Request $request, Response $response, $args)
     {
          $data = $request->getParsedBody();

          $data = $this->cursoService->guardarOactualizarGetData($data);

          $payload = json_encode($data);
          $response->getBody()->write($payload);

          return $response;
     }

     public function eliminar(Request $request, Response $response, $args)
     {
          $data = $request->getParsedBody();

          $idCurso = $data['id'];

          $data = $this->cursoService->eliminar($idCurso);

          $payload = json_encode($data);
          $response->getBody()->write($payload);

          return $response;
     }
}
