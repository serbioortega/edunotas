<?php

namespace Infrastructure\http\controller;

use Psr\Container\ContainerInterface;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Aplication\services\AsignaturaServiceInterface;


class AsignaturaController
{
     protected $container;
     protected $asignaturaService;

     public function __construct(ContainerInterface $container, AsignaturaServiceInterface $asignaturaService)
     {
          $this->container = $container;
          $this->asignaturaService = $asignaturaService;
     }

     public function listar(Request $request, Response $response, $args)
     {
          $data = $this->asignaturaService->listar();

          $payload = json_encode($data);
          $response->getBody()->write($payload);

          return $response;
     }

     public function recursos(Request $request, Response $response, $args)
     {
          $data = $request->getParsedBody();

          $recursos = $this->asignaturaService->obtenerRecursos($data);

          $payload = json_encode($recursos);
          $response->getBody()->write($payload);

          return $response;
     }

     public function guardar(Request $request, Response $response, $args)
     {
          $data = $request->getParsedBody();

          $data = $this->asignaturaService->guardarOactualizarGetData($data);

          $payload = json_encode($data);
          $response->getBody()->write($payload);

          return $response;
     }

     public function eliminar(Request $request, Response $response, $args)
     {
          $json = $request->getBody();
          $data = json_decode($json, true);

          $idAsignatura = $data['id'];

          $data = $this->asignaturaService->eliminar($idAsignatura);

          $payload = json_encode($data);
          $response->getBody()->write($payload);

          return $response;
     }
}
