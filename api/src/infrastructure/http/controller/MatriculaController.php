<?php

namespace Infrastructure\http\controller;

use Psr\Container\ContainerInterface;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Aplication\services\MatriculaServiceInterface;


class MatriculaController
{
     protected $container;
     protected $matriculaService;

     public function __construct(ContainerInterface $container, MatriculaServiceInterface $matriculaService)
     {
          $this->container = $container;
          $this->matriculaService = $matriculaService;
     }

     public function listar(Request $request, Response $response, $args)
     {
          $data = $request->getParsedBody();
          $data = $this->matriculaService->listar($data);

          $payload = json_encode($data);
          $response->getBody()->write($payload);

          return $response;
     }

     public function recursos(Request $request, Response $response, $args)
     {
          $data = $request->getParsedBody();

          $recursos = $this->matriculaService->obtenerRecursos($data);

          $payload = json_encode($recursos);
          $response->getBody()->write($payload);

          return $response;
     }

     public function guardar(Request $request, Response $response, $args)
     {
          $parametros = $request->getParsedBody();

          $data = $this->matriculaService->guardarOactualizar($parametros);

          $payload = json_encode($data);
          $response->getBody()->write($payload);

          return $response;
     }

     public function eliminar(Request $request, Response $response, $args)
     {
          $parametros = $request->getParsedBody();

          $data = $this->matriculaService->eliminar($parametros);

          $payload = json_encode($data);
          $response->getBody()->write($payload);

          return $response;
     }
}
