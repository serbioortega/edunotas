<?php

namespace Infrastructure\http\controller;

use Psr\Container\ContainerInterface;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Aplication\services\ColegioServiceInterface;


class ColegioController
{
     protected $container;
     protected $colegioService;

     public function __construct(ContainerInterface $container, ColegioServiceInterface $colegioService)
     {
          $this->container = $container;
          $this->colegioService = $colegioService;
     }

     public function listar(Request $request, Response $response, $args)
     {
          $data = $this->colegioService->listar();

          $payload = json_encode($data);
          $response->getBody()->write($payload);

          return $response;
     }

     public function recursos(Request $request, Response $response, $args)
     {
          $json = $request->getBody();
          $data = json_decode($json, true);

          $idColegio = isset($data['id']) ? $data['id'] : null;
          $recursos = $this->colegioService->obtenerRecursos($idColegio);

          $payload = json_encode($recursos);
          $response->getBody()->write($payload);

          return $response;
     }

     public function guardar(Request $request, Response $response, $args)
     {
          $json = $request->getBody();
          $data = json_decode($json, true);

          $colegio = $this->colegioService->guardarOactualizarGetData($data);

          $payload = json_encode(['registro' => 'exitoso', 'form' => $colegio]);
          $response->getBody()->write($payload);

          return $response;
     }

     public function eliminar(Request $request, Response $response, $args)
     {
          $json = $request->getBody();
          $data = json_decode($json, true);

          $idColegio = $data['id'];

          $data = $this->colegioService->eliminar($idColegio);

          $payload = json_encode($data);
          $response->getBody()->write($payload);

          return $response;
     }
}
