<?php

namespace Infrastructure\http\controller;

use Psr\Container\ContainerInterface;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Aplication\services\DocenteServiceInterface;


class DocenteController
{
     protected $container;
     protected $docenteService;

     public function __construct(ContainerInterface $container, DocenteServiceInterface $docenteService)
     {
          $this->container = $container;
          $this->docenteService = $docenteService;
     }

     public function listar(Request $request, Response $response, $args)
     {
          $data = $request->getParsedBody();
          $data = $this->docenteService->listar($data);

          $payload = json_encode($data);
          $response->getBody()->write($payload);

          return $response;
     }

     public function recursos(Request $request, Response $response, $args)
     {
          $data = $request->getParsedBody();

          $recursos = $this->docenteService->obtenerRecursos($data);

          $payload = json_encode($recursos);
          $response->getBody()->write($payload);

          return $response;
     }

     public function guardar(Request $request, Response $response, $args)
     {
          $parametros = $request->getParsedBody();

          $data = $this->docenteService->guardarOactualizar($parametros);

          $payload = json_encode($data);
          $response->getBody()->write($payload);

          return $response;
     }

     public function eliminar(Request $request, Response $response, $args)
     {
          $parametros = $request->getParsedBody();

          $data = $this->docenteService->eliminar($parametros);

          $payload = json_encode($data);
          $response->getBody()->write($payload);

          return $response;
     }
}
