<?php

namespace Infrastructure\http\controller;

use Psr\Container\ContainerInterface;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Aplication\services\MateriaServiceInterface;


class MateriaController
{
     protected $container;
     protected $materiaService;

     public function __construct(ContainerInterface $container, MateriaServiceInterface $materiaService)
     {
          $this->container = $container;
          $this->materiaService = $materiaService;
     }

     public function listar(Request $request, Response $response, $args)
     {
          $data = $request->getParsedBody();
          $data = $this->materiaService->listar($data);

          $payload = json_encode($data);
          $response->getBody()->write($payload);

          return $response;
     }

     public function recursos(Request $request, Response $response, $args)
     {
          $data = $request->getParsedBody();

          $recursos = $this->materiaService->obtenerRecursos($data);

          $payload = json_encode($recursos);
          $response->getBody()->write($payload);

          return $response;
     }

     public function guardar(Request $request, Response $response, $args)
     {
          $data = $request->getParsedBody();

          $materia = $this->materiaService->guardarOactualizarGetData($data);

          $payload = json_encode(['respuesta' => 'exitoso']);
          $response->getBody()->write($payload);

          return $response;
     }

     public function eliminar(Request $request, Response $response, $args)
     {
          $json = $request->getBody();
          $data = json_decode($json, true);

          $idMateria = $data['id'];

          $data = $this->materiaService->eliminar($idMateria);

          $payload = json_encode($data);
          $response->getBody()->write($payload);

          return $response;
     }
}
