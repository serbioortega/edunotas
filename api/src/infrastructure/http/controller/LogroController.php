<?php

namespace Infrastructure\http\controller;

use Psr\Container\ContainerInterface;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Aplication\services\LogroServiceInterface;


class LogroController
{
     protected $container;
     protected $logroService;

     public function __construct(ContainerInterface $container, LogroServiceInterface $logroService)
     {
          $this->container = $container;
          $this->logroService = $logroService;
     }

     public function listar(Request $request, Response $response, $args)
     {
          $data = $this->logroService->listar();

          $payload = json_encode($data);
          $response->getBody()->write($payload);

          return $response;
     }

     public function recursos(Request $request, Response $response, $args)
     {
          $json = $request->getBody();
          $data = json_decode($json, true);

          $idLogro = isset($data['id']) ? $data['id'] : null;
          $recursos = $this->logroService->obtenerRecursos($idLogro);

          $payload = json_encode($recursos);
          $response->getBody()->write($payload);

          return $response;
     }

     public function guardar(Request $request, Response $response, $args)
     {
          $json = $request->getBody();
          $data = json_decode($json, true);

          $logro = $this->logroService->guardarOactualizarGetData($data);

          $payload = json_encode(['registro' => 'exitoso', 'form' => $logro]);
          $response->getBody()->write($payload);

          return $response;
     }

     public function eliminar(Request $request, Response $response, $args)
     {
          $json = $request->getBody();
          $data = json_decode($json, true);

          $idLogro = $data['id'];

          $data = $this->logroService->eliminar($idLogro);

          $payload = json_encode($data);
          $response->getBody()->write($payload);

          return $response;
     }
}
