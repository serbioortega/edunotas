<?php


use Aplication\services\ColegioServiceInterface;
use Aplication\services\ColegioService;
use Aplication\infraestructure_interfaces\ColegioDaoInterface;
use Infrastructure\mysql\adapter\ColegioDao;


use Aplication\services\SedeServiceInterface;
use Aplication\services\SedeService;
use Aplication\infraestructure_interfaces\SedeDaoInterface;
use Infrastructure\mysql\adapter\SedeDao;


use Aplication\services\MunicipioServiceInterface;
use Aplication\services\MunicipioService;
use Aplication\infraestructure_interfaces\MunicipioDaoInterface;
use Infrastructure\mysql\adapter\MunicipioDao;



$container = $app->getContainer();



$container->set('Aplication\services\AreaServiceInterface', DI\get('Aplication\services\AreaService'));
$container->set('Aplication\infraestructure_interfaces\AreaDaoInterface', DI\get('Infrastructure\mysql\adapter\AreaDao'));

$container->set('Aplication\services\AsignaturaServiceInterface', DI\get('Aplication\services\AsignaturaService'));
$container->set('Aplication\infraestructure_interfaces\AsignaturaDaoInterface', DI\get('Infrastructure\mysql\adapter\AsignaturaDao'));

$container->set(ColegioDaoInterface::class, DI\get(ColegioDao::class));
$container->set(ColegioServiceInterface::class, DI\get(ColegioService::class));

$container->set('Aplication\services\CursoServiceInterface', DI\get('Aplication\services\CursoService'));
$container->set('Aplication\infraestructure_interfaces\CursoDaoInterface', DI\get('Infrastructure\mysql\adapter\CursoDao'));

$container->set('Aplication\services\DocenteServiceInterface', DI\get('Aplication\services\DocenteService'));
$container->set('Aplication\infraestructure_interfaces\DocenteDaoInterface', DI\get('Infrastructure\mysql\adapter\DocenteDao'));

$container->set('Aplication\services\GradoServiceInterface', DI\get('Aplication\services\GradoService'));
$container->set('Aplication\infraestructure_interfaces\GradoDaoInterface', DI\get('Infrastructure\mysql\adapter\GradoDao'));


$container->set('Aplication\infraestructure_interfaces\JornadaDaoInterface', DI\get('Infrastructure\mysql\adapter\JornadaDao'));


$container->set('Aplication\services\LogroServiceInterface', DI\get('Aplication\services\LogroService'));
$container->set('Aplication\infraestructure_interfaces\LogroDaoInterface', DI\get('Infrastructure\mysql\adapter\LogroDao'));

$container->set('Aplication\services\MateriaServiceInterface', DI\get('Aplication\services\MateriaService'));
$container->set('Aplication\infraestructure_interfaces\MateriaDaoInterface', DI\get('Infrastructure\mysql\adapter\MateriaDao'));

$container->set('Aplication\services\MatriculaServiceInterface', DI\get('Aplication\services\MatriculaService'));
$container->set('Aplication\infraestructure_interfaces\MatriculaDaoInterface', DI\get('Infrastructure\mysql\adapter\MatriculaDao'));

$container->set(MunicipioDaoInterface::class, DI\get(MunicipioDao::class));
$container->set(MunicipioServiceInterface::class, DI\get(MunicipioService::class));

$container->set(SedeDaoInterface::class, DI\get(SedeDao::class));
$container->set(SedeServiceInterface::class, DI\get(SedeService::class));

$container->set('Aplication\services\UsuarioServiceInterface', DI\get('Aplication\services\UsuarioService'));
$container->set('Aplication\infraestructure_interfaces\UsuarioDaoInterface', DI\get('Infrastructure\mysql\adapter\UsuarioDao'));

/*

$container->set('Aplication\infraestructure_interfaces\PedidoInterfaceDao', function () {
    return new PedidoDao();
});

 $container->set('Aplication\services\PedidoServiceInterface', function () {
    return new PedidoService(new PedidoDao());
}); 

$container->set('Aplication\infraestructure_interfaces\SedeDaoInterface', function () {
    return new SedeDao();
}); 

$container->set('Aplication\infraestructure_interfaces\ColegioDaoInterface', function () {
    return new ColegioDao();
}); 

$container->set('Aplication\services\ColegioServiceInterface', function () {
    return new ColegioService(new ColegioDao(), new SedeDao());
});


*/
