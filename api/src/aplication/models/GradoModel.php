<?php

namespace Aplication\models;

use Aplication\infraestructure_interfaces\GradoDaoInterface;

class GradoModel
{
    protected $gradoDao;

    public function __construct(GradoDaoInterface $gradoDao)
    {
        $this->gradoDao =  $gradoDao;
    }

    public function getDao()
    {
        return $this->gradoDao;
    }
}
