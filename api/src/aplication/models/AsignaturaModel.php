<?php

namespace Aplication\models;

use Aplication\infraestructure_interfaces\AsignaturaDaoInterface;

class AsignaturaModel
{
    protected $asignaturaDao;

    public function __construct(AsignaturaDaoInterface $asignaturaDao)
    {
        $this->asignaturaDao =  $asignaturaDao;
    }

    public function getDao()
    {
        return $this->asignaturaDao;
    }

    public function guardarOactualizar($data)
    {
        foreach ($data as $asignatura) {
            $esNuevo = empty($asignatura["id_asignatura"]);

            if ($esNuevo) {
                $idAsignatura = $this->asignaturaDao->guardarGetId($asignatura);
            } else {
                $idAsignatura = $this->asignaturaDao->actualizarGetId($asignatura);
            }
        }

        return;
    }
}
