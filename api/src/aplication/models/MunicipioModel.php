<?php

namespace Aplication\models;

use Aplication\infraestructure_interfaces\MunicipioDaoInterface;

class MunicipioModel
{
    protected $municipioDao;

    public function __construct(MunicipioDaoInterface $municipioDao)
    {
        $this->municipioDao =  $municipioDao;
    }

    public function getDao()
    {
        return $this->municipioDao;
    }

    public function listarDepartamentosYSusMunicipios()
    {
        $departamentosList = $this->municipioDao->todosDepartamentos();

        $resultado = [];

        foreach ($departamentosList as $departamento) {
            $data = ["codigo_departamento" => $departamento->codigo_departamento];

            $departamento->municipios = $this->municipioDao->municipiosDelDepartamento($data);
            $resultado[] = $departamento;
        }

        return $resultado;
    }
}
