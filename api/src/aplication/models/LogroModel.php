<?php

namespace Aplication\models;

use Aplication\infraestructure_interfaces\LogroDaoInterface;

class LogroModel
{
    protected $logroDao;

    public function __construct(LogroDaoInterface $logroDao)
    {
        $this->logroDao =  $logroDao;
    }

    public function getDao()
    {
        return $this->logroDao;
    }
}
