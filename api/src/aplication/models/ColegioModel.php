<?php

namespace Aplication\models;

use Aplication\infraestructure_interfaces\ColegioDaoInterface;

class ColegioModel
{
    protected $colegioDao;

    public function __construct(ColegioDaoInterface $colegioDao)
    {
        $this->colegioDao =  $colegioDao;
    }

    public function getDao()
    {
        return $this->colegioDao;
    }

    public function guardarOactualizarGetId($colegio)
    {
        $colegioNuevo = empty($colegio['id_colegio']);
        if ($colegioNuevo) {
            $idColegio = $this->colegioDao->guardarGetId($colegio);
        } else {
            $idColegio = $this->colegioDao->actualizarGetId($colegio);
        }
        return $idColegio;
    }
}
