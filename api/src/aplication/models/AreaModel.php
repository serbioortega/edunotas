<?php

namespace Aplication\models;

use Aplication\infraestructure_interfaces\AreaDaoInterface;

class AreaModel
{
    protected $areaDao;

    public function __construct(AreaDaoInterface $areaDao)
    {
        $this->areaDao =  $areaDao;
    }

    public function getDao()
    {
        return $this->areaDao;
    }
}
