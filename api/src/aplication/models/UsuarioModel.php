<?php

namespace Aplication\models;

use Aplication\infraestructure_interfaces\UsuarioDaoInterface;

class UsuarioModel
{
    protected $usuarioDao;

    public function __construct(UsuarioDaoInterface $usuarioDao)
    {
        $this->usuarioDao =  $usuarioDao;
    }

    public function getDao()
    {
        return $this->usuarioDao;
    }

    public function guardarOactualizar($usuario)
    {
        $usuarioNuevo = empty($usuario['id_usuario']);

        if ($usuarioNuevo) {
            $idUsuario = $this->usuarioDao->guardarGetId($usuario);
        } else {
            $idUsuario = $this->usuarioDao->actualizarGetId($usuario);
        }

        return $idUsuario;
    }
}
