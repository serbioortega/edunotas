<?php

namespace Aplication\models;

use Aplication\infraestructure_interfaces\MatriculaDaoInterface;

class MatriculaModel
{
    protected $matriculaDao;

    public function __construct(MatriculaDaoInterface $matriculaDao)
    {
        $this->matriculaDao =  $matriculaDao;
    }

    public function getDao()
    {
        return $this->matriculaDao;
    }
    
    public function guardarOactualizar($data)
    {
        $matriculaNuevo = empty($data['id_matricula']);

        if ($matriculaNuevo) {
            $idMatricula = $this->matriculaDao->guardarGetId($data);
        } else {
            $idMatricula = $this->matriculaDao->actualizarGetId($data);
        }

        return $idMatricula;
    }
}
