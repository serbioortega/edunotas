<?php

namespace Aplication\models;

use Aplication\infraestructure_interfaces\MateriaDaoInterface;

class MateriaModel
{
    protected $materiaDao;

    public function __construct(MateriaDaoInterface $materiaDao)
    {
        $this->materiaDao =  $materiaDao;
    }

    public function getDao()
    {
        return $this->materiaDao;
    }

    public function guardarOactualizar($data)
    {
        $materiaNuevo = empty($data['id_materia']);

        if ($materiaNuevo) {
            $idMateria = $this->materiaDao->guardarGetId($data);
        } else {
            $idMateria = $this->materiaDao->actualizarGetId($data);
        }

        return $idMateria;
    }



    public function guardarOactualizarOeliminarDetalle($data)
    {
        /* 
        id_colegio
        id_materia
        id_grado
        */

        $this->materiaDao->eliminarNotInDetalle(array_column($data, "id_materia_det"));

        foreach ($data as $detalle) {
            if (empty($detalle["id_materia_det"])) {
                $this->materiaDao->guardarDetalle($detalle);
            } else {
                $this->materiaDao->actualizarDetalle($detalle);
            }
        }
        /*
        eliminar
        actualiazar
        insertar
        */
    }
}
