<?php

namespace Aplication\models;

use Aplication\infraestructure_interfaces\CursoDaoInterface;

class CursoModel
{
    protected $cursoDao;

    public function __construct(CursoDaoInterface $cursoDao)
    {
        $this->cursoDao =  $cursoDao;
    }

    public function getDao()
    {
        return $this->cursoDao;
    }

    public function guardarOactualizar($curso)
    {
        $cursoNuevo = empty($curso['id_curso']);

        if ($cursoNuevo) {
            $idCurso = $this->cursoDao->guardarGetId($curso);
        } else {
            $idCurso = $this->cursoDao->actualizarGetId($curso);
        }

        return $idCurso;
    }
}
