<?php

namespace Aplication\models;

use Aplication\infraestructure_interfaces\JornadaDaoInterface;

class JornadaModel
{
    protected $jornadaDao;

    public function __construct(JornadaDaoInterface $jornadaDao)
    {
        $this->jornadaDao =  $jornadaDao;
    }

    public function getDao()
    {
        return $this->jornadaDao;
    }
}
