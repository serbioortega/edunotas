<?php

namespace Aplication\models;

use Aplication\infraestructure_interfaces\DocenteDaoInterface;

class DocenteModel
{
    protected $docenteDao;

    public function __construct(DocenteDaoInterface $docenteDao)
    {
        $this->docenteDao =  $docenteDao;
    }

    public function getDao()
    {
        return $this->docenteDao;
    }

    public function guardarOactualizar($data)
    {
        $docenteNuevo = empty($data['id_docente']);

        if ($docenteNuevo) {
            $idDocente = $this->docenteDao->guardarGetId($data);
        } else {
            $idDocente = $this->docenteDao->actualizarGetId($data);
        }

        return $idDocente;
    }
}
