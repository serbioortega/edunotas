<?php

namespace Aplication\models;

use Aplication\infraestructure_interfaces\SedeDaoInterface;

class SedeModel
{
    protected $sedeDao;

    public function __construct(SedeDaoInterface $sedeDao)
    {
        $this->sedeDao =  $sedeDao;
    }

    public function getDao()
    {
        return $this->sedeDao;
    }

    public function guardarOactualizarOeliminarSedes($data)
    {
        $sedesResultado = [];

        foreach ($data as $sede) {

            $sedeNueva = empty($sede['id_sede']);

            if ($sedeNueva) {
                unset($sede['id_sede']);
                $idSede = $this->sedeDao->guardarGetId($sede);
            } else {
                $sedeBd = $this->sedeDao->obtener($sede['id_sede']);

                $existeSede = empty($sedeBd) == false;

                if ($existeSede && $sedeBd->id_colegio == $sede["id_colegio"]) {
                    $idSede = $this->sedeDao->actualizarGetId($sede);
                } else {
                    throw new \Exception('Inconsistencia en la sede.');
                }
            }

            $sedesResultado[] = $idSede;
        }

        $this->sedeDao->eliminarSedesDiferentes($sedesResultado, $idColegio);

        return $sedesResultado;
    }
}
