<?php

namespace Aplication\services;

use Aplication\models\MatriculaModel;
use Aplication\models\UsuarioModel;
use Aplication\validations\MatriculaValidation;
use Aplication\validations\UsuarioValidation;


class MatriculaService implements MatriculaServiceInterface
{
    protected $matriculaModel;
    protected $usuarioModel;

    public function __construct(MatriculaModel $matriculaModel, UsuarioModel $usuarioModel)
    {
        $this->matriculaModel =  $matriculaModel;
        $this->usuarioModel =  $usuarioModel;
    }

    public function listar($data)
    {
        $matriculas = $this->matriculaModel->getDao()->todosPorColegio($data);
        return $matriculas;
    }

    public function obtenerRecursos($data)
    {
        $recursos = [];

        $matriculaNuevo = empty($data['id']);

        if ($matriculaNuevo == false) {
            $data["id_matricula"] = $data["id"];
            $matricula = $this->matriculaModel->getDao()->obtener($data);

            $recursos['form'] = $matricula;
        }

        return $recursos;
    }

    public function guardarOactualizar($data)
    {
        $idColegio = $data["id_colegio"];

        $matricula = MatriculaValidation::validar($data);

        $usuario = UsuarioValidation::validar($data);
        $usuario["id_colegio"] = $idColegio;

        $this->usuarioModel->getDao()->beginTransaction();

        if (empty($data["id_matricula"]) == false) {
            $matricula["id_matricula"] = $data["id_matricula"];

            $matriculaQuery = $this->matriculaModel->getDao()->obtener($data);
            $usuario["id_usuario"] = $matriculaQuery->id_usuario;
        }

        try {
            $usuario["rol"] = "alumno";
            $usuario["usuario"] = $matricula["identificacion"];
            $usuario["clave"] = $matricula["identificacion"];

            $idUsuario = $this->usuarioModel->guardarOactualizar($usuario);

            $matricula["id_usuario"] = $idUsuario;
            $idMatricula = $this->matriculaModel->guardarOactualizar($matricula);

            $this->usuarioModel->getDao()->commit();

            return ["mensaje" => "Registro exitoso"];
        } catch (\Exception $e) {
            $this->usuarioModel->getDao()->rollback();
            throw new \Exception($e->getMessage());
        }
    }

    public function eliminar($data)
    {
        $data["id_matricula"] = $data["id"];
        $matriculaQuery = $this->matriculaModel->getDao()->obtener($data);

        $idUsuario = $matriculaQuery->id_usuario;
        $data["id_usuario"] = $idUsuario;
        $this->matriculaModel->getDao()->eliminar($data);

        $this->usuarioModel->getDao()->eliminar($data);
    }
}
