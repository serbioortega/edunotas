<?php

namespace Aplication\services;

interface MateriaServiceInterface
{
    public function listar($data);
    public function obtenerRecursos($data);
    public function guardarOactualizarGetData($data);
    public function eliminar($data);
}
