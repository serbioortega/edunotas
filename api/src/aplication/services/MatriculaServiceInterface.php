<?php

namespace Aplication\services;

interface MatriculaServiceInterface
{
    public function listar($data);
    public function obtenerRecursos($data);
    public function guardarOactualizar($data);
    public function eliminar($data);
}
