<?php

namespace Aplication\services;

use Aplication\models\ColegioModel;
use Aplication\models\SedeModel;
use Aplication\models\MunicipioModel;
use Aplication\validations\ColegioValidation;
use Aplication\validations\SedeValidation;

class ColegioService implements ColegioServiceInterface
{
    protected $colegioModel;
    protected $sedeModel;
    protected $municipioModel;

    public function __construct(
        ColegioModel $colegioModel,
        SedeModel $sedeModel,
        MunicipioModel $municipioModel
    ) {
        $this->colegioModel =  $colegioModel;
        $this->sedeModel =  $sedeModel;
        $this->municipioModel =  $municipioModel;
    }

    public function listar()
    {
        $colegios = $this->colegioModel->getDao()->todos();
        return $colegios;
    }

    public function obtenerRecursos($parametros)
    {
        $recursos = [];

        if (empty($parametros['id_colegio']) == false) {
            $idColegio = $parametros['id_colegio'];
            $colegio = $this->colegioModel->getDao()->obtener($idColegio);
            $colegio->sedes =  $this->sedeModel->getDao()->obtenerSedesColegio($idColegio);

            $recursos['form'] = $colegio;
        }

        $departamentos = $this->municipioModel->listarDepartamentosYSusMunicipios();

        $recursos['departamentos'] = $departamentos;

        return $recursos;
    }

    public function guardarOactualizarGetData($data)
    {
        $colegio = ColegioValidation::validar($data);
        $sedes = SedeValidation::validarArray($data);

        $idColegio = $this->colegioModel->guardarOactualizarGetId($colegio);

        $this->sedeModel->guardarOactualizarOeliminarSedes($sedes, $idColegio);

        $colegio = $this->colegioModel->getDao()->obtener($idColegio);
        $colegio->sedes =  $this->sedeModel->getDao()->obtenerSedesColegio($idColegio);

        return $colegio;
    }

    public function eliminar($idColegio)
    {
        $this->colegioModel->getDao()->eliminar($idColegio);
    }
}
