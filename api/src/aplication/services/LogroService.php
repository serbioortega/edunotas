<?php

namespace Aplication\services;

use Aplication\models\LogroModel;
use Aplication\validations\LogroValidation;


class LogroService implements LogroServiceInterface
{
    protected $logroModel;

    public function __construct(LogroModel $logroModel)
    {
        $this->logroModel =  $logroModel;
    }

    public function listar($data)
    {
        $logros = $this->logroModel->getDao()->todos();
        return $logros;
    }

    public function obtenerRecursos($data)
    {
        $recursos = [];

        if (empty($data['id_logro']) == false) {
            $logro = $this->logroModel->getDao()->obtener($data);

            $recursos['form'] = $logro;
        }

        return $recursos;
    }

    public function guardarOactualizarGetData($data)
    {
        $logro = LogroValidation::validar($data);

        $idLogro = $this->logroModel->guardarOactualizarGetId($logro);

        return $idLogro;
    }

    public function eliminar($data)
    {
        $this->logroModel->getDao()->eliminar($data);
    }
}
