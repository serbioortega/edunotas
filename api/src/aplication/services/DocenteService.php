<?php

namespace Aplication\services;

use Aplication\models\DocenteModel;
use Aplication\models\UsuarioModel;
use Aplication\validations\DocenteValidation;
use Aplication\validations\UsuarioValidation;


class DocenteService implements DocenteServiceInterface
{
    protected $docenteModel;
    protected $usuarioModel;

    public function __construct(DocenteModel $docenteModel, UsuarioModel $usuarioModel)
    {
        $this->docenteModel =  $docenteModel;
        $this->usuarioModel =  $usuarioModel;
    }

    public function listar($data)
    {
        $docentes = $this->docenteModel->getDao()->todosPorColegio($data);
        return $docentes;
    }

    public function obtenerRecursos($data)
    {
        $recursos = [];

        $docenteNuevo = empty($data['id']);

        if ($docenteNuevo == false) {
            $data["id_docente"] = $data["id"];
            $docente = $this->docenteModel->getDao()->obtener($data);

            $recursos['form'] = $docente;
        }

        return $recursos;
    }

    public function guardarOactualizar($data)
    {
        $idColegio = $data["id_colegio"];

        $docente = DocenteValidation::validar($data);

        $usuario = UsuarioValidation::validar($data);
        $usuario["id_colegio"] = $idColegio;

        $this->usuarioModel->getDao()->beginTransaction();

        if (empty($data["id_docente"]) == false) {
            $docente["id_docente"] = $data["id_docente"];

            $docenteQuery = $this->docenteModel->getDao()->obtener($data);
            $usuario["id_usuario"] = $docenteQuery->id_usuario;
        }

        try {
            $usuario["rol"] = "docente";
            $usuario["usuario"] = $docente["identificacion"];
            $usuario["clave"] = $docente["identificacion"];

            $idUsuario = $this->usuarioModel->guardarOactualizar($usuario);

            $docente["id_usuario"] = $idUsuario;
            $idDocente = $this->docenteModel->guardarOactualizar($docente);

            $this->usuarioModel->getDao()->commit();

            return ["mensaje" => "Registro exitoso"];
        } catch (\Exception $e) {
            $this->usuarioModel->getDao()->rollback();
            throw new \Exception($e->getMessage());
        }
    }

    public function eliminar($data)
    {
        $data["id_docente"] = $data["id"];
        $docenteQuery = $this->docenteModel->getDao()->obtener($data);

        $idUsuario = $docenteQuery->id_usuario;
        $data["id_usuario"] = $idUsuario;
        $this->docenteModel->getDao()->eliminar($data);

        $this->usuarioModel->getDao()->eliminar($data);
    }
}
