<?php

namespace Aplication\services;

interface CursoServiceInterface
{
    public function listar($data);
    public function obtenerRecursos($data);
    public function guardarOactualizarGetData($data);
    public function eliminar($data);
}
