<?php

namespace Aplication\services;

use Aplication\models\CursoModel;
use Aplication\models\GradoModel;
use Aplication\models\JornadaModel;
use Aplication\models\SedeModel;
use Aplication\models\DocenteModel;
use Aplication\validations\CursoValidation;


class CursoService implements CursoServiceInterface
{
    protected $cursoModel;
    protected $gradoModel;
    protected $jornadaModel;
    protected $sedeModel;
    protected $docenteModel;

    public function __construct(CursoModel $cursoModel, GradoModel $gradoModel, JornadaModel $jornadaModel, SedeModel $sedeModel, DocenteModel $docenteModel)
    {
        $this->cursoModel =  $cursoModel;
        $this->gradoModel =  $gradoModel;
        $this->jornadaModel =  $jornadaModel;
        $this->sedeModel =  $sedeModel;
        $this->docenteModel =  $docenteModel;
    }

    public function listar($data)
    {
        $cursos = $this->cursoModel->getDao()->todosPorColegio($data);
        return $cursos;
    }

    public function obtenerRecursos($data)
    {
        $recursos = [];

        $recursos['grados'] = $this->gradoModel->getDao()->todos();
        $recursos['jornadas'] = $this->jornadaModel->getDao()->todos();
        $recursos['sedes'] = $this->sedeModel->getDao()->todosPorColegio($data);
        $recursos['docentes'] = $this->docenteModel->getDao()->todosPorColegio($data);

        if (empty($data['id']) == false) {
            $data['id_curso'] = $data['id'];
            $curso = $this->cursoModel->getDao()->obtener($data);
            $recursos['form'] = $curso;
        }

        return $recursos;
    }

    public function guardarOactualizarGetData($data)
    {
        $curso = CursoValidation::validar($data);
        $curso["id_colegio"] = $data["id_colegio"];
        $curso["ano"] = $data["ano"];

        $idCurso = $this->cursoModel->guardarOactualizar($curso);

        return ["mensaje" => "Registro exitoso"];
    }

    public function eliminar($data)
    {
        $this->cursoModel->getDao()->eliminar($data);
    }
}
