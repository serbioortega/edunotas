<?php

namespace Aplication\services;

use Aplication\models\AreaModel;
use Aplication\models\GradoModel;
use Aplication\models\MateriaModel;
use Aplication\validations\MateriaValidation;

use function PHPSTORM_META\map;

class MateriaService implements MateriaServiceInterface
{
    protected $materiaModel;
    protected $areaModel;
    protected $gradoModel;

    public function __construct(MateriaModel $materiaModel, AreaModel $areaModel, GradoModel $gradoModel)
    {
        $this->materiaModel =  $materiaModel;
        $this->areaModel =  $areaModel;
        $this->gradoModel =  $gradoModel;
    }

    public function listar($data)
    {
        $materias = $this->materiaModel->getDao()->todosPorColegio($data);
        return $materias;
    }

    public function obtenerRecursos($data)
    {
        $recursos = [];

        $materiaNuevo = empty($data['id']);

        $recursos['areas'] = $this->areaModel->getDao()->todos();
        $recursos['grados'] = $this->gradoModel->getDao()->todos();

        if ($materiaNuevo == false) {
            $data["id_materia"] = $data['id'];
            $docente = $this->materiaModel->getDao()->obtener($data);

            $docente->ihs = $this->materiaModel->getDao()->obtenerDetalle($data);
            $recursos['form'] = $docente;
        }

        return $recursos;
    }

    public function guardarOactualizarGetData($data)
    {
        $materia = MateriaValidation::validar($data);
        $materia["id_colegio"] = $data["id_colegio"];

        $idMateria = $this->materiaModel->guardarOactualizar($materia);

        $func = function ($item) use ($idMateria) {
            $item["id_materia"] = $idMateria;
            return $item;
        };

        $ihs = array_map($func, $data['ihs']);

        $this->materiaModel->guardarOactualizarOeliminarDetalle($ihs);

        return $idMateria;
    }

    public function eliminar($data)
    {
        $this->materiaModel->getDao()->eliminar($data);
    }
}
