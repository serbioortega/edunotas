<?php

namespace Aplication\services;

interface DocenteServiceInterface
{
    public function listar($data);
    public function obtenerRecursos($data);
    public function guardarOactualizar($data);
    public function eliminar($data);
}
