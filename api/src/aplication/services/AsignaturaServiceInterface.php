<?php

namespace Aplication\services;

interface AsignaturaServiceInterface
{
    public function listar($data);
    public function obtenerRecursos($data);
    public function guardarOactualizarGetData($data);
    public function eliminar($data);
}
