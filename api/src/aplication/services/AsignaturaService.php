<?php

namespace Aplication\services;

use Aplication\models\AsignaturaModel;
use Aplication\models\CursoModel;
use Aplication\models\DocenteModel;
use Aplication\models\JornadaModel;
use Aplication\models\MateriaModel;
use Aplication\models\SedeModel;
use Aplication\validations\AsignaturaValidation;


class AsignaturaService implements AsignaturaServiceInterface
{
    protected $asignaturaModel;
    protected $jornadaModel;
    protected $sedeModel;
    protected $docenteModel;
    protected $cursoModel;
    protected $materiaModel;

    public function __construct(AsignaturaModel $asignaturaModel, JornadaModel $jornadaModel, SedeModel $sedeModel, DocenteModel $docenteModel, CursoModel $cursoModel, MateriaModel $materiaModel)
    {
        $this->asignaturaModel =  $asignaturaModel;
        $this->jornadaModel =  $jornadaModel;
        $this->sedeModel =  $sedeModel;
        $this->docenteModel =  $docenteModel;
        $this->cursoModel =  $cursoModel;
        $this->materiaModel =  $materiaModel;
    }

    public function listar($data)
    {
        $asignaturas = $this->asignaturaModel->getDao()->todosPorColegio($data);
        return $asignaturas;
    }

    public function obtenerRecursos($data)
    {
        $recursos = [];

        $recursos['sedes'] = $this->sedeModel->getDao()->todosPorColegio($data);
        $recursos['jornadas'] = $this->jornadaModel->getDao()->todosConSede($data);
        $recursos['cursos'] = $this->cursoModel->getDao()->todosPorColegio($data);
        $recursos['asignaturas'] = $this->asignaturaModel->getDao()->todosPorColegio($data);

        $recursos['materias'] = $this->materiaModel->getDao()->todosPorColegio($data);
        $recursos['docentes'] = $this->docenteModel->getDao()->todosPorColegio($data);

        if (empty($data['id_asignatura']) == false) {
            $asignatura = $this->asignaturaModel->getDao()->obtener($data);

            $recursos['form'] = $asignatura;
        }

        return $recursos;
    }

    public function guardarOactualizarGetData($data)
    {
        $asignaturas = $data["asignaturas"];
        $idColegio = $data["id_colegio"];
        $func = function ($item) use ($idColegio) {
            $item["id_colegio"] = $idColegio;
            return $item;
        };

        $asignaturas = array_map($func, $asignaturas);

        $this->asignaturaModel->guardarOactualizar($asignaturas);

        return;
    }

    public function eliminar($data)
    {
        $this->asignaturaModel->getDao()->eliminar($data);
    }
}
