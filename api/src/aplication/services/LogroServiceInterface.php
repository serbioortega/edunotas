<?php

namespace Aplication\services;

interface LogroServiceInterface
{
    public function listar($data);
    public function obtenerRecursos($data);
    public function guardarOactualizarGetData($data);
    public function eliminar($data);
}
