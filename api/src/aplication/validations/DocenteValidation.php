<?php

namespace Aplication\validations;

use Aplication\validations\Validation;

class DocenteValidation extends Validation
{
    public $rules = [
        "nombres"               => "required",
        "apellidos"             => "required",
        "tipo_identificacion"   => "required",
        "identificacion"        => "required",
        "tipo_sangre"           => "",
        "titulo"                => "",
        "area_ensenanza"        => "",
    ];

    static function validar($data)
    {
        $v = new self();
        return $v->ejecutar($v->rules, $data);
    }
}
