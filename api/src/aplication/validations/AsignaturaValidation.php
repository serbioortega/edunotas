<?php

namespace Aplication\validations;

use Aplication\validations\Validation;

class AsignaturaValidation extends Validation
{
    public $rules = [
        "id_curso"      => "required",
        "id_materia"    => "required",
        "id_docente"    => ""
    ];

    static function validar($data)
    {
        $v = new self();
        return $v->ejecutar($v->rules, $data);
    }
}
