<?php

namespace Aplication\validations;

use Aplication\validations\Validation;

class SedeValidation extends Validation
{
    public $rules = [
        "nombre_sede" => "required"
    ];

    static function validar($data)
    {
        $v = new self();
        return $v->ejecutar($v->rules, $data);
    }

    static function validarArray($data)
    {
        if (empty($data['sedes'])) {
            throw new \Exception("Debe agregar por lo menos una sede", 422);
        }

        $v = new self();
        $sedes = [];
        foreach ($data as $sede) {
            $sedes[] = $v->ejecutar($v->rules, $sede);
        }
        return $sedes;
    }
}
