<?php

namespace Aplication\validations;

use Aplication\validations\Validation;

class ColegioValidation extends Validation
{
    public $rules = [
        "nombre_colegio"                => "required",
        "alias"                         => "required",
        "departamento"                  => "required",
        "municipio"                     => "required",
        "dane"                          => "required",
        "resolucion"                    => "required",
        "nombre_rector"                 => "required",
        "identificacion_rector"         => "required",
        "nombre_secretario"             => "required",
        "identificacion_secretario"     => "required",
        "sedes"                         => "required"
    ];

    static function validar($data)
    {
        $v = new self();
        return $v->ejecutar($v->rules, $data);
    }
}
