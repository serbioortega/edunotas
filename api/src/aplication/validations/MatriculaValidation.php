<?php

namespace Aplication\validations;

use Aplication\validations\Validation;

class MatriculaValidation extends Validation
{
    public $rules = [
        "nombres"           => "required",
        "apellidos"           => "required",
        "tipo_identificacion"    => "required",
        "identificacion"    => "required",
        "estado"            => "required",
        "direccion"         => "required",
        "celular"         => "required",
        "correo"         => "required",
        "fecha_nacimiento"  => "required",
        "tipo_sangre"         => "required"
    ];

    static function validar($data)
    {
        $v = new self();
        return $v->ejecutar($v->rules, $data);
    }
}
