<?php

namespace Aplication\validations;

class Validation
{
    public $rules;

    function ejecutar($rulesParam, $data)
    {
        //$a = 0 / 0;
        $result = [];
        $errores = [];
        foreach ($rulesParam as $key => $rules) {
            $arrayRules = explode("|", $rules);
            foreach ($arrayRules as $rule) {
                $arrayRule = explode(":", $rule);
                $metodo = $arrayRule[0];
                $param = array_slice($arrayRule, 1);

                if (array_key_exists($key, $data)) {
                    $p = array_merge(["valor" => $data[$key]], $param);

                    $r = $metodo == "" ? $metodo : call_user_func_array([$this, $metodo], $p);
                    if ($r == "") {
                        $result[$key] = $data[$key];
                    } else {
                        $errores[$key] = $r;
                    }
                } else {
                    throw new \Exception("Data incorrecta, no se encontro la propiedad '" . $key . "'", 500);
                }
            }
        }

        if (count($errores) > 0) {
            throw new \Exception(json_encode($errores), 422);
        } else {
            return $result;
        }
    }


    public function required($valor)
    {
        if ($valor != "") {
            return "";
        } else {
            return "Es requerido";
        }
    }

    public function length($valor, $min, $max)
    {
        if (strlen($valor) >= $min && strlen($valor) <= $max) {
            return "";
        } else {
            return "Length";
        }
    }
}
