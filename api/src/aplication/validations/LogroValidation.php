<?php

namespace Aplication\validations;

use Aplication\validations\Validation;

class LogroValidation extends Validation
{
    public $rules = [
        "id_materia"    => "required",
        "descripcion"   => "required"
    ];

    static function validar($data)
    {
        $v = new self();
        return $v->ejecutar($v->rules, $data);
    }
}
