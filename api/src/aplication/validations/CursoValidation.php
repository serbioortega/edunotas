<?php

namespace Aplication\validations;

use Aplication\validations\Validation;

class CursoValidation extends Validation
{
    public $rules = [
        "id_curso"      => "",
        "id_colegio"    => "required",
        "id_sede"       => "required",
        "id_jornada"    => "required",
        "id_grado"      => "required",
        "id_docente"    => "required",
        "grupo"         => "required",
        "ano"           => "required"
    ];

    static function validar($data)
    {
        $v = new self();
        return $v->ejecutar($v->rules, $data);
    }
}
