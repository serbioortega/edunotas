<?php

namespace Aplication\validations;

use Aplication\validations\Validation;

class MateriaValidation extends Validation
{
    public $rules = [
        "id_materia"        => "",
        "nombre_materia"    => "required",
        "id_area"           => "required"
    ];

    static function validar($data)
    {
        $v = new self();
        return $v->ejecutar($v->rules, $data);
    }
}
