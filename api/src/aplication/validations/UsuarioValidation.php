<?php

namespace Aplication\validations;

use Aplication\validations\Validation;

class UsuarioValidation extends Validation
{
    public $rules = [
        "fecha_nacimiento"  => "",
        "sexo"              => "",
        "direccion"         => "",
        "celular"           => "",
        "correo"            => "",
        "estado"            => ""
    ];

    static function validar($data)
    {
        $v = new self();
        return $v->ejecutar($v->rules, $data);
    }
}
