<?php

namespace Aplication\infraestructure_interfaces;

interface JornadaDaoInterface
{
    public function todos();
    public function todosConSede($data);
    public function obtener($data);
}
