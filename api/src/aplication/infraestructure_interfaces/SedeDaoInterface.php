<?php

namespace Aplication\infraestructure_interfaces;

interface SedeDaoInterface
{
    public function todosPorColegio($data);
    public function obtener($data);
    public function guardarGetId($data);
    public function actualizarGetId($data);
    public function eliminarSedesDiferentes($data);
}
