<?php

namespace Aplication\infraestructure_interfaces;

interface MunicipioDaoInterface
{
    public function todosDepartamentos();
    public function municipiosDelDepartamento($data);
}
