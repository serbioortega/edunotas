<?php

namespace Aplication\infraestructure_interfaces;

interface AreaDaoInterface
{
    public function todos();
    public function obtener($data);
}
