<?php

namespace Aplication\infraestructure_interfaces;

interface AsignaturaDaoInterface
{
    public function todosPorColegio($data);
    public function obtener($data);
    public function guardarGetId($data);
    public function actualizarGetId($data);
    public function eliminar($data);
}
