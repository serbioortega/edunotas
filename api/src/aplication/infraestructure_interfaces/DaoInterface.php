<?php

namespace Aplication\infraestructure_interfaces;

interface DaoInterface
{
    public function beginTransaction();
    public function commit();
    public function rollback();
}
