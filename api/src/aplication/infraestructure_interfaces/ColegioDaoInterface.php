<?php

namespace Aplication\infraestructure_interfaces;

interface ColegioDaoInterface
{
    public function todos();
    public function obtener($data);
    public function guardarGetId($data);
    public function actualizarGetId($data);
    public function eliminar($data);
}
