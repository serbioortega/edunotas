<?php

namespace Aplication\infraestructure_interfaces;

interface GradoDaoInterface
{
    public function todos();
    public function obtener($data);
}
