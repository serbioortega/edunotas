<?php

namespace Aplication\infraestructure_interfaces;

interface MateriaDaoInterface
{
    public function todosPorColegio($data);
    public function obtener($data);
    public function guardarGetId($data);
    public function actualizarGetId($data);

    public function obtenerDetalle($data);
    public function guardarDetalle($data);
    public function actualizarDetalle($data);

    public function eliminarNotInDetalle($data);


    public function eliminar($data);
}
