<?php

error_reporting(E_ALL);
ini_set('display_errors', '0');


use DI\ContainerBuilder;
use Slim\Factory\AppFactory;

use  Infrastructure\database\Manager as DB;
use Infrastructure\mysql\MyPDO;
use Psr\Container\ContainerInterface;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Server\RequestHandlerInterface as RequestHandler;
use Slim\Psr7\Response;

require __DIR__ . '/vendor/autoload.php';


$db = new DB;

$db->addConnection([
    'driver'    => 'mysql',
    'host'      => 'anapestanadiputada.com',
    //'host'      => 'localhost',
    'database'  => 'anapesta_devel',
    'username'  => 'anapesta_devel',
    'password'  => 'anapesta_devel',
    'charset'   => 'utf8',
    'collation' => 'utf8_unicode_ci',
    'prefix'    => '',
]);


// This will Capsule instance available globally via static methods
$db->setAsGlobal();

// Setup the Eloquent ORM
$db->bootEloquent();


$containerBuilder = new ContainerBuilder();

$containerBuilder->addDefinitions(
    [
        MyPDO::class => function (ContainerInterface $container) {
            $host = "anapestanadiputada.com";
            $dbname =  "anapesta_devel";
            $username = "anapesta_devel";
            $password = "anapesta_devel";
            $charset = "utf8";
            $flags = [
                // Turn off persistent connections
                PDO::ATTR_PERSISTENT => false,
                // Enable exceptions
                PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
                // Emulate prepared statements
                PDO::ATTR_EMULATE_PREPARES => true,
                // Set default fetch mode to array
                PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
                // Set character set
                PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8 COLLATE utf8_unicode_ci'
            ];
            $dsn = "mysql:host=$host;dbname=$dbname;charset=$charset";

            return new MyPDO($dsn, $username, $password, $flags);
        }
    ]
);

AppFactory::setContainer($containerBuilder->build());

$app = AppFactory::create();

$app->add(function (Request $request, RequestHandler $handler) {
    $contentType = $request->getHeaderLine('Content-Type');

    if (strstr($contentType, 'application/json')) {
        $contents = json_decode(file_get_contents('php://input'), true);
        if (json_last_error() === JSON_ERROR_NONE) {

            $contents["id_colegio"] = 1;
            $contents["ano"] = 2019;

            $request = $request->withParsedBody($contents);
        }
    }

    return $handler->handle($request);
});

$app->add(function (Request $request, RequestHandler $handler) {

    if ($request->getMethod() == "OPTIONS") {
        $response = new Response();
    } else {
        $response = $handler->handle($request);
    }

    return $response->withHeader('Content-Type', 'application/json')
        ->withStatus(200)
        ->withHeader('Access-Control-Allow-Origin', '*')
        ->withHeader('Access-Control-Allow-Headers', 'Content-Type, X-Auth-Token, Origin, Authorization')
        ->withHeader('Access-Control-Allow-Methods', 'POST, GET, OPTIONS, PUT, DELETE');
});

if ($_SERVER['SERVER_PORT'] != 8888) {
    $app->setBasePath('/devel/api');
}

require __DIR__ . '/src/setup/ErrorHandler.php';

require __DIR__ . '/src/setup/dependencies.php';

require __DIR__ . '/src/infrastructure/http/routes.php';

$app->run();
