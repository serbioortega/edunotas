-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost:3306
-- Tiempo de generación: 27-09-2019 a las 12:26:51
-- Versión del servidor: 10.1.41-MariaDB-cll-lve
-- Versión de PHP: 7.2.7



-- SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
-- SET AUTOCOMMIT = 0;
-- START TRANSACTION;
-- SET time_zone = "+00:00";



/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `edunotas_dev`
--

-- --------------------------------------------------------



DROP TABLE IF EXISTS `en_colegios`;
CREATE TABLE `en_colegios` (
  `id_colegio`                int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `nombre_colegio`            varchar(100) NOT NULL,
  `alias`                     varchar(20) NOT NULL,
  `departamento`              int(2) UNSIGNED NOT NULL,
  `municipio`                 int(4) UNSIGNED NOT NULL,
  `dane`                      varchar(20) NOT NULL,
  `resolucion`                TEXT NULL,
  `nombre_rector`             varchar(100) NOT NULL,
  `identificacion_rector`     varchar(100) NULL,
  `nombre_secretario`         varchar(100) NULL,
  `identificacion_secretario` varchar(100) NULL,
  `estado`                    ENUM('activo', 'inactivo') DEFAULT 'activo',
  `created_at`                timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at`                timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                              PRIMARY KEY (`id_colegio`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


-- --------------------------------------------------------



DROP TABLE IF EXISTS `en_anos_lectivos`;
CREATE TABLE `en_anos_lectivos` (
  `id_ano_lectivo`            int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `id_colegio`                int(10) UNSIGNED NOT NULL,
  `ano`                       int(4) UNSIGNED NOT NULL,
  `actual`                    ENUM('si', 'no') DEFAULT 'no',
  `desempeño_bajo`            varchar(20) NOT NULL,
  `desempeño_basico`          varchar(20) NOT NULL,
  `desempeño_alto`            varchar(20) NOT NULL,
  `desempeño_superior`        varchar(20) NOT NULL,
  `planilla_asistencia`       varchar(20) NOT NULL,
  `planilla_valoracion`       varchar(20) NOT NULL,
  `certificado_estudio`       varchar(20) NOT NULL,
  `informe`                   varchar(20) NOT NULL,
  `carnet`                    varchar(20) NOT NULL,
  `created_at`                timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at`                timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                              PRIMARY KEY (`id_ano_lectivo`),
                              UNIQUE `id_colegio_ano_uidx` (`id_colegio`,`ano`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


-- --------------------------------------------------------


DROP TABLE IF EXISTS `en_sedes`;
CREATE TABLE `en_sedes` (
  `id_sede`     int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `id_colegio`  int(10) UNSIGNED NOT NULL,
  `nombre_sede` varchar(100) NOT NULL,
  `direccion`   varchar(100) NULL,
  `celular`     int(10) UNSIGNED NULL,
  `correo`      varchar(100) NULL,
  `estado`      ENUM('activo', 'inactivo') DEFAULT 'activo',
  `created_at`  timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at`  timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                PRIMARY KEY (`id_sede`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



-- --------------------------------------------------------



DROP TABLE IF EXISTS `en_usuarios`;
CREATE TABLE `en_usuarios` (
  `id_usuario`        int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `id_colegio`        int(10) UNSIGNED NOT NULL,
  `usuario`           varchar(20) NOT NULL,
  `clave`             varchar(20) NULL,
  `fecha_nacimiento`  varchar(20) NULL,
  `sexo`              varchar(20) NULL,
  `direccion`         varchar(10) NULL,
  `celular`           int(10) UNSIGNED NULL,
  `correo`            varchar(100) NULL,
  `estado`            ENUM('activo', 'inactivo') NOT NULL DEFAULT 'activo',
  `rol`               ENUM('administrador', 'docente', 'alumno') NOT NULL,
  `created_at`        timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at`        timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                      PRIMARY KEY (`id_usuario`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


-- --------------------------------------------------------



DROP TABLE IF EXISTS `en_docentes`;
CREATE TABLE `en_docentes` (
  `id_docente`          int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `id_usuario`          int(10) UNSIGNED NOT NULL,
  `nombres`             varchar(60) NOT NULL,
  `apellidos`           varchar(60) NOT NULL,
  `tipo_identificacion`  ENUM('CC', 'RC', 'TI'),
  `identificacion`      varchar(60) NOT NULL,
  `tipo_sangre`         varchar(60)  NULL,
  `titulo`              varchar(60)  NULL,
  `area_ensenanza`      varchar(60)  NULL,
  `created_at`          timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at`          timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                        PRIMARY KEY (`id_docente`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


-- --------------------------------------------------------




DROP TABLE IF EXISTS `en_jornadas`;
CREATE TABLE `en_jornadas` (
  `id_jornada`  int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `nombre_jornada`      varchar(20) NOT NULL,
  `created_at`  timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at`  timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                PRIMARY KEY (`id_jornada`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



-- --------------------------------------------------------


DROP TABLE IF EXISTS `en_grados`;
CREATE TABLE `en_grados` (
  `id_grado`    int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `nombre_grado`      varchar(30) NOT NULL,
  `nivel`       varchar(30) NOT NULL,
  `created_at`  timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at`  timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                PRIMARY KEY (`id_grado`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



-- --------------------------------------------------------



DROP TABLE IF EXISTS `en_cursos`;
CREATE TABLE `en_cursos` (
  `id_curso`    int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `id_colegio`  int(10) UNSIGNED NOT NULL,
  `id_sede`     int(10) UNSIGNED NOT NULL,
  `id_jornada`  int(10) UNSIGNED NOT NULL,
  `id_grado`    int(10) UNSIGNED NOT NULL,
  `id_docente`  int(10) UNSIGNED NOT NULL,
  `grupo`       varchar(50) NOT NULL,
  `ano`         int(4) UNSIGNED NOT NULL,
  `created_at`  timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at`  timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                PRIMARY KEY (`id_curso`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


-- --------------------------------------------------------



DROP TABLE IF EXISTS `en_matriculas`;
CREATE TABLE `en_matriculas` (
  `id_matricula`      int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `id_colegio`        int(10) UNSIGNED NOT NULL,
  `id_curso`          int(10) UNSIGNED NOT NULL,
  `id_usuario`        int(10) UNSIGNED NOT NULL,
  `nombres`           varchar(60) NOT NULL,
  `apellidos`           varchar(60) NOT NULL,
  `tipo_identificacion`  ENUM('CC', 'RC', 'TI'),
  `identificacion`    varchar(60) NOT NULL,
  `estado`            ENUM('masculino', 'femenino'),
  `direccion`         varchar(100) NULL,
  `celular`           int(10) UNSIGNED NULL,
  `correo`            varchar(100) NULL,
  `fecha_nacimiento`  date,
  `tipo_sangre`       varchar(60) NOT NULL,

  `created_at`        timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at`        timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                      PRIMARY KEY (`id_matricula`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


-- --------------------------------------------------------



DROP TABLE IF EXISTS `en_areas`;
CREATE TABLE `en_areas` (
  `id_area`     int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `nombre_area`      varchar(100) NOT NULL,
  `created_at`  timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at`  timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                PRIMARY KEY (`id_area`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;




-- --------------------------------------------------------



DROP TABLE IF EXISTS `en_materias`;
CREATE TABLE `en_materias` (
  `id_materia`    int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `id_colegio`    int(10) UNSIGNED NOT NULL,
  `id_area`       int(10) UNSIGNED NOT NULL,
  `nombre_materia`varchar(100) NOT NULL,
  `created_at`    timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at`    timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                  PRIMARY KEY (`id_materia`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


-- --------------------------------------------------------



DROP TABLE IF EXISTS `en_materias_det`;
CREATE TABLE `en_materias_det` (
  `id_materia_det`  int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `id_materia`      int(10) UNSIGNED NOT NULL,
  `id_grado`        int(10) UNSIGNED NOT NULL,
  `ihs`             int(10) UNSIGNED NOT NULL,
  `created_at`      timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at`      timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                    PRIMARY KEY (`id_materia_det`),
                    UNIQUE (`id_materia`, `id_grado`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


-- --------------------------------------------------------


DROP TABLE IF EXISTS `en_asignaturas`;
CREATE TABLE `en_asignaturas` (
  `id_asignatura` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `id_colegio`    int(10) UNSIGNED NOT NULL,
  `id_curso`      int(10) UNSIGNED NOT NULL,
  `id_materia`    int(10) UNSIGNED NOT NULL,
  `id_docente`    int(10) UNSIGNED NOT NULL,
  `created_at`    timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at`    timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                  PRIMARY KEY (`id_asignatura`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


-- --------------------------------------------------------



DROP TABLE IF EXISTS `en_logros`;
CREATE TABLE `en_logros` (
  `id_logro`    int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `id_materia`  int(10) UNSIGNED NOT NULL,
  `id_grado`      int(10) UNSIGNED NOT NULL,
  `descripcion` TEXT NOT NULL,
  `created_at`  timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at`  timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                PRIMARY KEY (`id_logro`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


-- --------------------------------------------------------



DROP TABLE IF EXISTS `en_notas`;
CREATE TABLE `en_notas` (
  `id_nota`       int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `id_matricula`  int(10) UNSIGNED NOT NULL,
  `id_materia`    int(10) UNSIGNED NOT NULL,
  `id_logro`      int(10) UNSIGNED NOT NULL,
  `periodo`       int(1) UNSIGNED NOT NULL,
  `nota`          decimal(3,2),
  `created_at`    timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at`    timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                  PRIMARY KEY (`id_nota`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


-- --------------------------------------------------------



DROP TABLE IF EXISTS `en_observaciones`;
CREATE TABLE `en_observaciones` (
  `id_observacion`  int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `id_matricula`    int(10) UNSIGNED NOT NULL,
  `periodo`         int(1) UNSIGNED NOT NULL,
  `observaciones`   TEXT NOT NULL,
  `created_at`      timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at`      timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                    PRIMARY KEY (`id_observacion`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


-- --------------------------------------------------------



DROP TABLE IF EXISTS `en_fallas`;
CREATE TABLE `en_fallas` (
  `id_falla`      int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `id_matricula`  int(10) UNSIGNED NOT NULL,
  `id_materias`   int(10) UNSIGNED NOT NULL,
  `periodo`       int(1) UNSIGNED NOT NULL,
  `fallas`        int(3) UNSIGNED NOT NULL,
  `created_at`    timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at`    timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                  PRIMARY KEY (`id_falla`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


-- --------------------------------------------------------



DROP TABLE IF EXISTS `en_municipios`;
CREATE TABLE `en_municipios` (
  `id_municipio`        int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `codigo_municipio`    varchar(5) NOT NULL,
  `municipio`           varchar(27) NOT NULL,
  `codigo_departamento` varchar(2) NOT NULL,
  `departamento`        varchar(28) NOT NULL,
  `created_at`          timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at`          timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                        PRIMARY KEY (`id_municipio`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


-- --------------------------------------------------------


CREATE OR REPLACE VIEW view_departamentos AS
  SELECT distinct codigo_departamento,departamento
  FROM en_municipios;


-- --------------------------------------------------------


CREATE OR REPLACE VIEW view_cursos AS
 SELECT
    c.id_curso,
    CONCAT(g.nombre_grado, " ", c.grupo) AS curso,
    j.nombre_jornada,
    s.nombre_sede,
    CONCAT(d.nombres, " ", d.apellidos) AS docente
FROM en_cursos c
JOIN en_grados g ON g.id_grado = c.id_grado
JOIN en_jornadas j ON j.id_jornada = c.id_jornada
JOIN en_sedes s ON s.id_sede = c.id_sede
JOIN en_docentes d ON d.id_docente = c.id_docente;


-- --------------------------------------------------------
CREATE OR REPLACE VIEW view_asignaturas AS
 SELECT
    a.id_asignatura,
    a.id_materia,
    m.nombre_materia,
    a.id_docente,
    CONCAT(d.nombres, " ", d.apellidos) AS docente,
    a.id_curso
FROM en_asignaturas a
JOIN en_materias m ON m.id_materia = a.id_materia
LEFT JOIN en_docentes d ON d.id_docente = a.id_docente;


-- --------------------------------------------------------


-- COMMIT;



/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
