<?php

function testSql($db){

    $colegios = array(
        [
            'nombre_colegio'    => 'C.E. EL PITAL',
            'alias'             => 'PITAL',
            'departamento'      => '70',
            'municipio'         => '70708',
            'dane'              => '270708000334',
            'nombre_rector'     => 'GRISALDO JOSE FLOREZ FLOREZ'
        ],
        [
            'nombre_colegio'    => 'I.E. TÉCNICO AGROPECUARIO EL LIMON',
            'alias'             => 'LIMON',
            'departamento'      => '70',
            'municipio'         => '70708',
            'dane'              => '170708000364',
            'nombre_rector'     => 'LEDYS RAMOS CHIQUILLO'
        ],
        [
            'nombre_colegio'    => 'CENTRO EDUCATIVO BOCA PUERTA',
            'alias'             => 'BOCA PUERTA',
            'departamento'      => '70',
            'municipio'         => '70708',
            'dane'              => '270708000261',
            'nombre_rector'     => 'MAGALYS ISABEL ACOSTA MORENO'
        ],
    );

    foreach($colegios as $colegio){
        $db->table('en_colegios')->insertGetId($colegio);
    }


}