import {
    required
} from 'vuelidate/lib/validators';

export default {
    id_materia: {
        value: '',
        validation: {
              required
        }
    },
    descripcion: {
        value: '',
        validation: {
               required
        }
    }
};
