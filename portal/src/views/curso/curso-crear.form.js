import {
    required
} from 'vuelidate/lib/validators';

export default {
    id_sede: {
        value: '',
        validation: {
            required
        }
    },
    id_jornada: {
        value: '',
        validation: {
            required
        }
    },
    id_grado: {
        value: '',
        validation: {
            required
        }
    },
    grupo: {
        value: '',
        validation: {
            required
        }
    },
    id_docente: {
        value: '',
        validation: {
            required
        }
    },
};
