import {
    required
} from 'vuelidate/lib/validators';

export default {
    nombre: {
        value: '',
        validation: {
            required
        }
    },
    nivel: {
        value: '',
        validation: {
            required
        }
    }
};
