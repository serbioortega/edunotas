import {
    required
} from 'vuelidate/lib/validators';

export default {
    id_sede: {
        value: '',
        validation: {
            required
        }
    },
    id_jornada: {
        value: '',
        validation: {
            required
        }
    },
    id_curso: {
        value: '',
        validation: {
            required
        }
    },
    asignaturas: {
        value: [],
        validation: {
        }
    }
};
