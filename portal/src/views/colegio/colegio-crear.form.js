import {
    required
} from 'vuelidate/lib/validators';

export default {
    nombre_colegio: {
        value: '',
        validation: {
            required
        }
    },
    alias: {
        value: '',
        validation: {
            required
        }
    },
    departamento: {
        value: '',
        validation: {
            required
        }
    },
    municipio: {
        value: '',
        validation: {
            required
        }
    },
    dane: {
        value: '',
        validation: {
            required
        }
    },
    resolucion: {
        value: '',
        validation: {
            required
        }
    },
    nombre_rector: {
        value: '',
        validation: {
            required
        }
    },
    identificacion_rector: {
        value: '',
        validation: {
            required
        }
    },
    nombre_secretario: {
        value: '',
        validation: {
            required
        }
    },
    identificacion_secretario: {
        value: '',
        validation: {
            required
        }
    },
    sedes: {
        value: [],
        validation: {

        }
    }
};
