import {
    required
} from 'vuelidate/lib/validators';

export default {
    nombre_sede: {
        value: '',
        validation: {
            required
        }
    },
    direccion: {
        value: '',
        validation: {
            required
        }
    },
    celular: {
        value: '',
        validation: {
            required
        }
    },
    correo: {
        value: '',
        validation: {
            required
        }
    },
    estado: {
        value: true,
        validation: {
            required
        }
    }
};
