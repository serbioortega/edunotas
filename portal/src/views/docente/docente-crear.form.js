import {
    required
} from 'vuelidate/lib/validators';

export default {
    nombres: {
        value: '',
        validation: {
            required
        }
    },
    apellidos: {
        value: '',
        validation: {
            required
        }
    },
    tipo_identificacion: {
        value: '',
        validation: {
            required
        }
    },
    identificacion: {
        value: '',
        validation: {
            required
        }
    },
    fecha_nacimiento: {
        value: undefined,
        validation: {
            required
        }
    },
    sexo: {
        value: '',
        validation: {
            required
        }
    },
    tipo_sangre: {
        value: '',
        validation: {
            //  required
        }
    },
    direccion: {
        value: '',
        validation: {
            //     required
        }
    },
    celular: {
        value: '',
        validation: {
            //    required
        }
    },
    correo: {
        value: '',
        validation: {
            required
        }
    },
    titulo: {
        value: '',
        validation: {
            //required
        }
    },
    area_ensenanza: {
        value: '',
        validation: {
            //required
        }
    },
    estado: {
        value: true,
        validation: {
            required
        }
    }
};
