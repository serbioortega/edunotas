import {
    required
} from 'vuelidate/lib/validators';

export default {
    id_materia: {
        value: 0,
        validation: {
            
        }
    },
    nombre_materia: {
        value: '',
        validation: {
            required
        }
    },
    id_area: {
        value: '',
        validation: {
            required
        }
    },

};
