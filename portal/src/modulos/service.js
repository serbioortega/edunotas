import axios from 'axios'

class Service {

    static obtenerRecursos(url, data, vm) {

        axios.post(url, data).then((response) => {

            let respuesta = response.data;

            let detener = vm.recursosPreExitoso(respuesta) == false;
            if (detener) {
                return;
            }



            for (let prop in respuesta) {

                if (prop == 'form' && (respuesta[prop].length == 0 || respuesta[prop] == undefined)) {
                    respuesta[prop] = Object.assign({}, this.cloneForm);
                }

                vm.$data[prop] = respuesta[prop];

            }

            vm.recursosPostExitoso(response);

        }).catch(error => {

            let detener = vm.recursosPreError(error) == false;
            if (detener) {
                return;
            }

            vm.$message.error("Error en el servidor", 15);

            vm.recursosPostError(error);
        });
    }

    static guardar(url, data, vm) {

        axios.post(url, data).then((response) => {

            let data = response.data;
            let detener = vm.guardarPreExitoso(data) == false;
            if (detener) {
                return;
            }

            if (vm.esNuevo) {
                vm.form = Object.assign({}, vm.cloneFormDefault());
            }

            vm.$message.success(data.respuesta);

            vm.guardarPostExitoso(data);

        }).catch(error => {

            let detener = vm.guardarPreError(error) == false;
            if (detener) {
                return;
            }

            if (error.response && error.response.status == 422) {

                let errores = error.response.data.errors;
                for (const e in errores) {
                    vm.$message.error(errores[e][0], 15);
                }
            } else {
                vm.$message.error("Error en el servidor, " + error.response.data.error.message, 15);
            }

            vm.guardarPostError(error);
        }).finally(() => {
            vm.guardando = false;
        });
    }

    static eliminar(url, data, vm) {

        axios.post(url, data).then((response) => {

            let detener = vm.eliminarPreExitoso(response) == false;
            if (detener) {
                return;
            }

            vm.$message.success("Registro eliminado");
            vm.obtenerListado();

            vm.eliminarPostExitoso(response);

        }).catch(error => {

            let detener = vm.eliminarPreError(error) == false;
            if (detener) {
                return;
            }

            vm.$message.error("Error en el servidor", 15);

            vm.eliminarPostError(error);
        });
    }

    static listar(url, data, vm) {
        vm.cargando = true;
        axios.post(url, data).then((response) => {

            let detener = vm.listarPreExitoso(response) == false;
            if (detener) {
                return;
            }

            vm.data = response.data;
            vm.dataListar = response.data;

            vm.listarPostExitoso(response);

        }).catch(error => {

            let detener = vm.listarPreError(error) == false;
            if (detener) {
                return;
            }
            vm.$message.error("Error en el servidor", 15);

            vm.listarPostError(error);
        }).finally(() => {
            vm.cargando = false;
        });
    }
}

export {
    Service,
    axios
};