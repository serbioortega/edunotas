export default {
    mostrarError: function (field) {
        return field.$error ? "error" : "";
    },
    mensajeError: function (field, label) {
        if (field.$error) {
            if (field.required == false) {
                return label + " es obligatorio.";
            } else if (field.numeric == false) {
                return label + " debe ser numerico.";
            } else if (field.isDate == false) {
                return label + " debe ser una fecha valida.";
            }
        }
        return "";
    }
}