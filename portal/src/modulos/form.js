export default {
    set data(data) {
        this._data = data;
    },
    get form() {
        var form = {}
        for (const key in this._data) {
            form[key] = this._data[key].value;
        }
        return form;
    },
    get validations() {
        var form = {}
        for (const key in this._data) {
            if (this._data[key].validation == undefined) {
                this._data[key].validation = {};
            }
            form[key] = this._data[key].validation;
        }
        return form;
    }
}