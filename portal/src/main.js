import Vue from 'vue'
import App from './App.vue'

Vue.config.productionTip = false

if (process.env.NODE_ENV === "production") {
  axios.defaults.baseURL = 'https://anapestanadiputada.com/devel/api/';
} else if(process.env.NODE_ENV === "127"){
  axios.defaults.baseURL = 'http://127.0.0.1:8888/';
  window.config.menus = [];
}else{
  axios.defaults.baseURL = 'http://localhost/devel/api/';
}

import VueAxios from 'vue-axios';
import axios from 'axios';
Vue.use(VueAxios, axios);

import Antd from 'ant-design-vue'
import 'ant-design-vue/dist/antd.less'


Vue.use(Antd)

import router from './router'

import './assets/main.css'


import layout from './components/layout/layout.vue';
Vue.component('app-layout', layout);

import layoutContent from './components/layout/layout-content.vue';
Vue.component('app-layout-content', layoutContent);

import formItem from './components/form-item.vue';
Vue.component('app-form-item', formItem);

import select from './components/select.vue';
Vue.component('app-select', select);

import columnTitle from './components/column-title.vue';
Vue.component('app-column-title', columnTitle);


new Vue({
  render: h => h(App),
  router,
}).$mount('#app')