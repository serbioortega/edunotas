import Vue from 'vue'
import Router from 'vue-router'
import Inicio from './../views/Inicio.vue'

Vue.use(Router)

let rutas = [{
    path: '/',
    component: Inicio
}];

let entidades = ['docente', 'curso', 'materia', 'asignatura', 'logro', 'matricula', 'grado'];

for (const entidad of entidades) {

    rutas.push({
        path: `/${entidad}`,
        component: () => import( /* webpackChunkName: "view-[request]" */ `./../views/${entidad}/${entidad}-listar.vue`)
    });

    rutas.push({
        path: `/${entidad}/crear`,
        component: () => import( /* webpackChunkName: "view-[request]" */ `./../views/${entidad}/${entidad}-crear.vue`)
    });

    rutas.push({
        path: `/${entidad}/editar/:id`,
        component: () => import( /* webpackChunkName: "view-[request]" */ `./../views/${entidad}/${entidad}-crear.vue`)
    });
}

rutas.push({
    path: `/configuracion`,
    component: () => import( /* webpackChunkName: "view-[request]" */ `./../views/configuracion/establecer-ano.vue`)
});


export default new Router({
    mode: 'history',
    base: '/devel/dist/',
    routes: rutas
})