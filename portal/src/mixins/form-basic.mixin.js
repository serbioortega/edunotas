import {
    validationMixin
} from 'vuelidate'
import formService from '../modulos/form.js';

export default {
    mixins: [validationMixin],
    data: function () {
        return {
            formData: {},
            form: {},
            validations: {},
            esNuevo: true
        }
    },
    created: function () {

        formService.data = this.formData;

        this.form = formService.form;
        this.validations = formService.validations;
    },
    validations() {
        return {
            form: this.validations
        }
    },
    methods: {
        validarForm: function () {

            this.$v.$touch();
            if (this.$v.$invalid) {
                var invalidos = "";
                for (const prop in this.$v.form) {
                    if (this.$v.form[prop].$invalid) {
                        invalidos = invalidos + " - " + prop;
                    }
                }

                this.$message.error("Revise las validaciones.", 10);
                return false;
            }
            return true;
        },
        validar: function () {
            return this.validarForm();
        },
        cloneFormDefault: function () {
            return formService.form;
        }
    }
}
