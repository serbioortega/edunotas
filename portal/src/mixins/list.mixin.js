import {
    Service
} from './../modulos/service.js';

export default {
    data: function () {
        return {
            urlListar: '',
            urlEliminar: '',
            data: [],
            cargando: false,
            columnas: [],
            widthAcciones: 190, //100
            filtros: {},
            dataListar: [],
            locale: {
                emptyText: 'No se encontraron resultado'
            }
        }
    },
    mounted: function () {

    },
    created: function () {
        this.columnas.unshift({
            slots: {
                title: "customAcciones"
            },
            dataIndex: "",
            key: "x",
            width: this.widthAcciones,
            scopedSlots: {
                customRender: "action"
            }
        });
        this.obtenerListado();
    },
    methods: {

        listarParams: function () {
            return {};
        },
        listarPreExitoso: function (respuesta) {
            return true;
        },
        listarPostExitoso: function (respuesta) {
            return true;
        },
        listarPreError: function (respuesta) {
            return true;
        },
        listarPostError: function (respuesta) {
            return true;
        },
        obtenerListado: function () {

            let parametros = this.listarParams();
            let detener = parametros == undefined
            if (detener) {
                return;
            }

            Service.listar(this.urlListar, parametros, this)
        },
        editar: function (id) {
            this.$router.push(this.urlEditar + '/' + id);
        },
        eliminarPreExitoso: function (respuesta) {
            return true;
        },
        eliminarPostExitoso: function (respuesta) {
            return true;
        },
        eliminarPreError: function (respuesta) {
            return true;
        },
        eliminarPostError: function (respuesta) {
            return true;
        },
        eliminar: function (id) {

            var r = confirm("Confirma que desea eliminar el registro?");
            if (r == true) {
                Service.eliminar(this.urlEliminar, {
                    id: id
                }, this);
            }
        },
        buscar: function (e, d) {
            const value = e.target.value;
            if (value == "") {
                delete this.filtros[d];
            } else {
                this.filtros[d] = value;
            }

            if (this.filtros.length == 0) {
                this.dataListar = this.data;
                return;
            }

            this.dataListar = this.data.filter(item => {
                let r = true;
                for (const key in this.filtros) {
                    if (item[key].toLowerCase().includes(this.filtros[key].toLowerCase()) == false) {
                        r = false;
                        break;
                    }
                }
                return r;
            });
        }
    }
}