import {
    Service
} from './../modulos/service.js';

import formBasicMixin from './form-basic.mixin.js';

export default {
    mixins: [formBasicMixin],
    data: function () {
        return {
            urlGuardar: '',
            urlRecursos: '',
            guardando: false
        }
    },
    created: function () {

        if (this.$route.params.id != undefined) {
            this.esNuevo = false;
        }

        this.recursos();
    },
    methods: {

        guardarParams: function () {
            return this.form;
        },
        guardarPreExitoso: function (respuesta) {
            return true;
        },
        guardarPostExitoso: function (respuesta) {
            return true;
        },
        guardarPreError: function (respuesta) {
            return true;
        },
        guardarPostError: function (respuesta) {
            return true;
        },
        guardar: function () {

            this.guardando = true;
            if (this.validar() == false) {
                this.guardando = false;
                return;
            }

            let parametros = this.guardarParams();
            let detener = parametros == undefined
            if (detener) {
                this.guardando = false;
                return;
            }

            Service.guardar(this.urlGuardar, parametros, this);
        },
        recursosParams: function () {
            return this.esNuevo ? undefined : { id: this.$route.params.id };
        },
        recursosPreExitoso: function (respuesta) {
            return true;
        },
        recursosPostExitoso: function (respuesta) {
            return true;
        },
        recursosPreError: function (respuesta) {
            return true;
        },
        recursosPostError: function (respuesta) {
            return true;
        },
        recursos: function () {

            let parametros = this.recursosParams();
            let detener = parametros == undefined
            if (detener) {
                return;
            }

            Service.obtenerRecursos(this.urlRecursos, parametros, this)
        }
    }
}
