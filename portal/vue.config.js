// vue.config.js
const path = require('path')
const prod = process.env.NODE_ENV === "production";

module.exports = {
    publicPath: prod ? '/devel/dist/' : '/',
    outputDir: '../dist',
    indexPath: prod ? 'index.php' : '/index.html',
    filenameHashing: false, //nombre sin hash app.6k4m7m.js
    productionSourceMap: false, // No map
    configureWebpack: { //un solo app
        optimization: {
            splitChunks: false
        }
    },
    devServer: {
        overlay: {
            // warnings: true,
            // errors: true
        },
        // proxy: "http://anapestanadiputada.com/devel/public/api"
        // proxy: "http://localhost/devel/api"
    },
    /* chainWebpack: config => {
        const types = ['vue-modules', 'vue', 'normal-modules', 'normal']
        types.forEach(type => addStyleResource(config.module.rule('stylus').oneOf(type)))
    }, */
    css: {
        loaderOptions: {
            less: {
                modifyVars: {
                    'border-color-base': '#b3b3b3',
                    'border-color-split': '#b3b3b3',
                },
                javascriptEnabled: true
            }
        }
    }
}

function addStyleResource(rule) {
    rule.use('style-resource')
        .loader('style-resources-loader')
        .options({
            patterns: [
                path.resolve(__dirname, './src/styles/imports.styl'),
            ],
        })
}